# code generator

## 使用步骤
1. 将所有文件分别放到项目代码对应目录，pom.xml里的依赖添加到项目pom.xml中
2. 编写自定义Bean，参考Bean.java文件，修改config.json中bean的配置项
3. 配置生成文件的路径，
4. 配置完成后运行Generator程序即可
5. 生成文件路径在配置的路径

~~~json
{
  "bean":"com.bhxj.entity.Bean",
  "outRootDir":"",

  "sqlDir":"src/main/resources/sql",
  "mapperDir":"src/main/resources/mybatis",
  "serviceDir":"src/main/java/com/bhxj/service",
  "daoDir":"src/main/java/com/bhxj/repository/mybatis"
}
~~~

## Bean编写
1. 如果没有用Table注解，则使用bean的名字全小写为table名称
2. 可以用Id注解设置自增主键，或者Column注解的primary设置字符串主键
3. Column注解的length属性是表示数据库列的类型长度
4. Column注解的nullable属性可以指定列是否可为空
5. Column注解的unique还未处理


## 版本信息
- 2015-10-23 初始版本，通过配置来生成


## 示例
Decimal字段示例
~~~java
@Column(name = "current_money", nullable = true, decimalLength = "12,2")
private double currentMoney;
~~~

Text字段示例
~~~java
@Column(columnDefinition = "TEXT", length = 1024)
private String story;
~~~

