-- MySQL dump 10.13  Distrib 5.1.63, for apple-darwin10.3.0 (i386)
--
-- Host: localhost    Database: bhxj
-- ------------------------------------------------------
-- Server version	5.1.63

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coupon_t`
--
CREATE DATABASE `bhxj` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `bhxj`;

DROP TABLE IF EXISTS `coupon_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_t` (
  `uuid` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主键',
  `user_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ALL' COMMENT '用户ID',
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '优惠卷描述',
  `code` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '优惠码',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '何种类型的优惠，比如满减，包邮，直减',
  `start_time` datetime DEFAULT NULL COMMENT '优惠卷起效时间',
  `end_time` datetime DEFAULT NULL COMMENT '优惠卷停用时间',
  PRIMARY KEY (`uuid`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='优惠配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_t`
--

LOCK TABLES `coupon_t` WRITE;
/*!40000 ALTER TABLE `coupon_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_job_t`
--

DROP TABLE IF EXISTS `employee_job_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_job_t` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '员工id',
  `job_date` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '工作日期 默认数据由operation表得出',
  `plan_hour` decimal(11,0) DEFAULT NULL COMMENT '额定小时数 默认数据由operation表得出',
  `real_hour` decimal(10,1) DEFAULT NULL COMMENT '实际小时数 默认数据由operation表得出但可修改',
  `operation_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '对应流水表ID',
  `create_by` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_by` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operationId` (`operation_id`),
  KEY `employeeId` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_job_t`
--

LOCK TABLES `employee_job_t` WRITE;
/*!40000 ALTER TABLE `employee_job_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_job_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_t`
--

DROP TABLE IF EXISTS `employee_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_t` (
  `id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '员工编号',
  `name` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
  `telephone` int(11) DEFAULT NULL COMMENT '个人电话',
  `sex` int(1) DEFAULT NULL COMMENT '性别',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `birthday` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '生日',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '员工住址',
  `hometown` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '户口所在地 亲人及联系方式',
  `identification` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '身份证号',
  `type` int(2) DEFAULT NULL COMMENT '0运维 1保洁',
  `contract_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '合同号',
  `hour_salary` decimal(10,2) DEFAULT NULL COMMENT '每小时薪资',
  `basic_salary` decimal(10,2) DEFAULT NULL COMMENT '基本工资',
  `insurance_salary` decimal(10,2) DEFAULT NULL COMMENT '保险基数',
  `max_morning_hour` decimal(10,2) DEFAULT NULL COMMENT '上午最多工作小时',
  `max_afternoon_hour` decimal(10,2) DEFAULT NULL COMMENT '下午最多工作小时',
  `max_night_hour` decimal(10,2) DEFAULT NULL COMMENT '晚间最多工作小时',
  `join_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '入职时间',
  `leave_date` int(11) DEFAULT NULL COMMENT '离职时间',
  `status` int(1) DEFAULT NULL COMMENT '0在职 1假期 2离职',
  `level` int(1) DEFAULT NULL COMMENT '1 钻石 2金 3银 4铜',
  `discount` decimal(10,1) DEFAULT NULL COMMENT '1.1 1 0.9 0.8 0.7 0.6',
  `month_bonus` int(11) DEFAULT NULL COMMENT '月度奖金额 可算作电话费和车费补助',
  `year_bonus` int(11) DEFAULT NULL COMMENT '年度奖金',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_t`
--

LOCK TABLES `employee_t` WRITE;
/*!40000 ALTER TABLE `employee_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `express_t`
--

DROP TABLE IF EXISTS `express_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `express_t` (
  `uuid` varchar(20) NOT NULL DEFAULT '' COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '快递公司名',
  `contact_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人',
  `telephone` bigint(11) DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='合作快递公司表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `express_t`
--

LOCK TABLES `express_t` WRITE;
/*!40000 ALTER TABLE `express_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `express_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods_bunding_t`
--

DROP TABLE IF EXISTS `goods_bunding_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_bunding_t` (
  `id` int(40) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_id` varchar(40) DEFAULT '' COMMENT '物品id',
  `bunding_id` varchar(40) DEFAULT NULL COMMENT '绑定优惠的商品id',
  `title` varchar(200) DEFAULT NULL COMMENT '绑定优惠的物品标题',
  `image` varchar(200) DEFAULT NULL COMMENT '绑定商品的图片',
  `nprice` decimal(10,2) DEFAULT NULL COMMENT '绑定商品的现价',
  `price` decimal(10,2) DEFAULT NULL COMMENT '绑定商品的优惠价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_bunding_t`
--

LOCK TABLES `goods_bunding_t` WRITE;
/*!40000 ALTER TABLE `goods_bunding_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `goods_bunding_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods_image_t`
--

DROP TABLE IF EXISTS `goods_image_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_image_t` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` varchar(40) NOT NULL DEFAULT '' COMMENT '商品ID',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'name' COMMENT '图片名称路径',
  PRIMARY KEY (`Id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品图片表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_image_t`
--

LOCK TABLES `goods_image_t` WRITE;
/*!40000 ALTER TABLE `goods_image_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `goods_image_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods_t`
--

DROP TABLE IF EXISTS `goods_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_t` (
  `uuid` varchar(40) NOT NULL DEFAULT '' COMMENT '商品主键',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '商品标题',
  `title_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '标题标识',
  `goods_style` int(2) NOT NULL COMMENT '0单次 1包月1次 2包月2次 4包月4次 8包月8次',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '商品描述',
  `market_price` decimal(10,2) DEFAULT '0.00' COMMENT '市场价',
  `current_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '现价',
  `save_price` decimal(10,2) DEFAULT '0.00' COMMENT '为您节省的',
  `status` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '商品上架下架状态 未上架 上架 已下架',
  `discount` decimal(10,2) DEFAULT '0.00' COMMENT '折扣额',
  `sale_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品上架时间',
  `end_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品下架时间',
  `amount` int(11) DEFAULT NULL COMMENT '商品个数',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图片名称',
  `count_sale` int(11) DEFAULT '0' COMMENT '已销售数量',
  `stock_sum` int(11) DEFAULT '0' COMMENT '总库存',
  `stock_remain` int(11) DEFAULT '0' COMMENT '剩余库存，由销售商品时总量减销售量得出',
  `extra_content` text COMMENT '存储编辑框的html内容',
  `supplier_id` varchar(20) DEFAULT NULL COMMENT '供货商ID',
  `tprice` varchar(10) DEFAULT NULL COMMENT '该商品邮费',
  `hot` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '热卖 0非热卖 1热卖',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT '商品定位',
  `collect_count` int(11) NOT NULL DEFAULT '0' COMMENT '商品收藏个数',
  `reward` int(2) DEFAULT '0' COMMENT '是否为抽奖商品：0为不是，1为是',
  `delivery` int(2) DEFAULT '1' COMMENT '是否为自提商品：1为配送，0为自提',
  `create_by` varchar(40) DEFAULT NULL COMMENT '由谁创建',
  `create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品录入时间',
  `update_by` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_time` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '专属客户',
  `address_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '专属地址',
  PRIMARY KEY (`uuid`),
  KEY `style` (`goods_style`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_t`
--

LOCK TABLES `goods_t` WRITE;
/*!40000 ALTER TABLE `goods_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `goods_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operation_t`
--

DROP TABLE IF EXISTS `operation_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_t` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `order_id` varchar(40) DEFAULT NULL,
  `order_detail_id` varchar(40) DEFAULT NULL,
  `operation_date` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '工作日期',
  `plan_start_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '计划开始时间',
  `plan_end_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '计划结束时间',
  `plan_hour` decimal(10,1) DEFAULT NULL COMMENT '计划工作小时数',
  `plan_price` decimal(10,2) DEFAULT NULL COMMENT '计划收取费用',
  `real_start_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '实际开始时间',
  `real_end_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '实际结束时间',
  `real_hour` decimal(10,1) DEFAULT NULL COMMENT '实际工作小时数',
  `real_price` decimal(10,2) DEFAULT NULL COMMENT '实际收取费用',
  `pay_status` int(2) DEFAULT NULL COMMENT '0未付款 1已付款',
  `job_status` int(2) DEFAULT NULL COMMENT '0未完成 1已完成 2进行中 3暂停 4终止',
  `evaluate` decimal(10,1) DEFAULT NULL COMMENT '10,9.5,9,8,7',
  `evaluate_content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '评价内容',
  `owe_employee_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '如果现金收款人',
  PRIMARY KEY (`id`),
  KEY `userId` (`user_id`),
  KEY `goodsId` (`goods_id`),
  KEY `orderId` (`order_id`),
  KEY `odId` (`order_detail_id`),
  KEY `oweId` (`owe_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation_t`
--

LOCK TABLES `operation_t` WRITE;
/*!40000 ALTER TABLE `operation_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `operation_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail_t`
--

DROP TABLE IF EXISTS `order_detail_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail_t` (
  `uuid` varchar(40) NOT NULL DEFAULT '' COMMENT '订单明细主键UUID',
  `order_id` varchar(40) NOT NULL DEFAULT '' COMMENT '对应主订单',
  `goods_id` varchar(40) NOT NULL DEFAULT '' COMMENT '对应商品',
  `goods_title` varchar(500) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `goods_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品图片',
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '购买此商品的数量',
  `address_detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT ' ' COMMENT '收货详细地址',
  `supplier_id` varchar(20) DEFAULT NULL COMMENT '供货商ID',
  `express_id` varchar(20) DEFAULT NULL COMMENT '快递ID',
  `express_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '快递公司名',
  `express_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '快递单号',
  `send_status` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '未发货 已发货 完成',
  `send_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货时间',
  `send_detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '快递记录',
  `tprice` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单对应详细商品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail_t`
--

LOCK TABLES `order_detail_t` WRITE;
/*!40000 ALTER TABLE `order_detail_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_t`
--

DROP TABLE IF EXISTS `order_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_t` (
  `uuid` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '购买主键，流水号',
  `user_id` varchar(40) DEFAULT NULL COMMENT '购买者ID',
  `price` decimal(10,2) DEFAULT NULL COMMENT '成交价格',
  `real_price` decimal(10,2) DEFAULT NULL,
  `tprice` decimal(10,2) DEFAULT NULL COMMENT '订单总运费',
  `coupon_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '优惠卷ID',
  `coupon_price` decimal(10,2) DEFAULT NULL COMMENT '优惠卷金额',
  `address_id` varchar(50) DEFAULT NULL COMMENT '送货地址',
  `address_detail` varchar(500) DEFAULT ' ' COMMENT '送货地址明细',
  `buy_time` varchar(20) DEFAULT NULL COMMENT '购买时间',
  `send_time` varchar(20) DEFAULT NULL COMMENT '发货时间',
  `send_status` varchar(3) NOT NULL DEFAULT '' COMMENT '未发货 已发货 完成',
  `order_status` varchar(3) DEFAULT NULL COMMENT '支付状态 已支付 未支持 等待',
  `pay_way` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '付款方式 支付宝 网银 微信 现金',
  `pay_way_detail` varchar(10) DEFAULT NULL COMMENT '支付宝中方式 网银方式',
  `shopping_count` int(11) NOT NULL DEFAULT '0' COMMENT '购买数量',
  `delivery_by_self_yn` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'y自提 n派送',
  `owe_employee_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '费用由公司员工代领',
  `best_employee_id` varchar(40) DEFAULT NULL COMMENT '最佳表现员工',
  PRIMARY KEY (`uuid`),
  KEY `user_id` (`user_id`),
  KEY `addressId` (`address_id`),
  KEY `oweId` (`owe_employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物历史表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_t`
--

LOCK TABLES `order_t` WRITE;
/*!40000 ALTER TABLE `order_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_t`
--

DROP TABLE IF EXISTS `price_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_t` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `type` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '业务类型',
  `price` decimal(11,2) NOT NULL COMMENT '单价',
  `worker_salary` decimal(11,2) DEFAULT NULL COMMENT '保洁员提成基数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_t`
--

LOCK TABLES `price_t` WRITE;
/*!40000 ALTER TABLE `price_t` DISABLE KEYS */;
INSERT INTO `price_t` VALUES (1,'普通保洁按小时收费','clean','25.00','20.00'),(2,'单擦玻璃按小时收费','glass','35.00','25.00'),(3,'全套大扫除按小时收费','sweep','30.00','25.00'),(4,'皮革养护按座位收费','leather','50.00','30.00'),(5,'地板养护按平米收费','floor','0.00',NULL),(6,'石材养护按平米收费','ceramic','0.00',NULL),(7,'地毯清洗按平米收费','carpet','0.00',NULL),(8,'厨房护理按间收费','kitchen','200.00',NULL),(9,'卫生间护理按间收费','restroom','200.00',NULL);
/*!40000 ALTER TABLE `price_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_t`
--

DROP TABLE IF EXISTS `reward_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_t` (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '抽奖ID',
  `user_id` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '用户ID',
  `goods_id` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '作为抽奖商品的id',
  `goods_title` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '作为抽奖商品的名称',
  `goods_image` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '抽奖商品的图片',
  `telephone` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '用户手机\r\n\r\n号码',
  `prize_number` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '抽奖号\r\n\r\n码',
  `create_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '抽奖录入的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_t`
--

LOCK TABLES `reward_t` WRITE;
/*!40000 ALTER TABLE `reward_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_t`
--

DROP TABLE IF EXISTS `supplier_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_t` (
  `uuid` varchar(20) NOT NULL DEFAULT '' COMMENT '主键',
  `sys_username` varchar(50) NOT NULL DEFAULT '' COMMENT '系统登录ID',
  `sys_pwd` varchar(50) DEFAULT NULL COMMENT '系统登录密码',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '供货商名字',
  `contact_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人姓名',
  `telephone` bigint(11) DEFAULT NULL COMMENT '联系人手机号',
  `p` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供货商所在地省市',
  `c` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供货商所在地区县',
  `d` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供货商所在地区',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供货商详细地址',
  `postcode` int(10) DEFAULT NULL COMMENT '供货人邮编',
  `start_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '开始合作时间',
  `end_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '终止合作时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='有的呢供货商表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_t`
--

LOCK TABLES `supplier_t` WRITE;
/*!40000 ALTER TABLE `supplier_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address_t`
--

DROP TABLE IF EXISTS `user_address_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_address_t` (
  `Id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `p` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省',
  `c` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '市',
  `d` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区',
  `properties` int(11) DEFAULT NULL COMMENT '小区',
  `detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '具体门牌',
  `size` decimal(10,2) DEFAULT NULL COMMENT '建筑面积',
  `real_size` decimal(10,2) DEFAULT NULL COMMENT '实用面积',
  `people` int(5) DEFAULT NULL COMMENT '居住人数',
  `postcode` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '邮编',
  `telephone` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号码',
  `receiver` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货人',
  PRIMARY KEY (`Id`),
  KEY `user_id` (`user_id`),
  KEY `placeid` (`properties`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户收货地址';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address_t`
--

LOCK TABLES `user_address_t` WRITE;
/*!40000 ALTER TABLE `user_address_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_collect_t`
--

DROP TABLE IF EXISTS `user_collect_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_collect_t` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(40) DEFAULT NULL COMMENT '用户ID',
  `goods_id` varchar(40) DEFAULT NULL COMMENT '商品ID',
  `create_time` varchar(20) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`Id`),
  KEY `user_id` (`user_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户商品收藏表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_collect_t`
--

LOCK TABLES `user_collect_t` WRITE;
/*!40000 ALTER TABLE `user_collect_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_collect_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_recommend_t`
--

DROP TABLE IF EXISTS `user_recommend_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_recommend_t` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '被邀请人',
  `recommend_user_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '推荐人',
  `buy_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '第一次购买商品金额',
  `register_time` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册时间',
  `gift_yn` tinyint(1) DEFAULT '0' COMMENT '未获奖 已派送',
  PRIMARY KEY (`id`),
  KEY `re_index_id` (`recommend_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_recommend_t`
--

LOCK TABLES `user_recommend_t` WRITE;
/*!40000 ALTER TABLE `user_recommend_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_recommend_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_t`
--

DROP TABLE IF EXISTS `user_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_t` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '主键',
  `telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '注册手机号',
  `pwd` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '别名',
  `sex` int(1) DEFAULT NULL COMMENT '0男 1女',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `birthday` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '生日',
  `job_id` int(11) DEFAULT NULL COMMENT '从事职业',
  `company` int(11) DEFAULT NULL COMMENT '就职公司',
  `register_time` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户注册时间',
  `login_time` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '最后登录时间',
  `status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户状态 未激活 活跃 停用',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户邮箱',
  `subscribe` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮件订阅信息',
  `current_money` decimal(10,2) unsigned zerofill NOT NULL COMMENT '当前帐户余额',
  `level` int(2) DEFAULT NULL COMMENT '1银牌一级 2银牌二级 3银牌三级 4 5 6 7 8 9 ',
  PRIMARY KEY (`id`),
  KEY `telephone` (`telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='购物者表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_t`
--

LOCK TABLES `user_t` WRITE;
/*!40000 ALTER TABLE `user_t` DISABLE KEYS */;
INSERT INTO `user_t` VALUES ('0','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'00000000.00',NULL);
/*!40000 ALTER TABLE `user_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_trade_t`
--

DROP TABLE IF EXISTS `user_trade_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_trade_t` (
  `id` varchar(40) NOT NULL DEFAULT '' COMMENT '充值流水 日期时间毫秒随机数',
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用户',
  `money` decimal(10,2) NOT NULL COMMENT '此业务金额',
  `type` int(2) NOT NULL COMMENT '0充值缴费 1消费 2退款',
  `order_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '对应订单号',
  `time` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '交易时间',
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_trade_t`
--

LOCK TABLES `user_trade_t` WRITE;
/*!40000 ALTER TABLE `user_trade_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_trade_t` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-14 10:46:54
