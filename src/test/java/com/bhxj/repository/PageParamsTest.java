package com.bhxj.repository;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by Jean on 7/9/15.
 */
public class PageParamsTest extends TestCase {
    private PageParams pageParams;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        pageParams = new PageParams(1, 2);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

    }

    @Test
    public void testGetOffset() throws Exception {
        assertEquals(0, pageParams.getOffset());
    }

    @Test
    public void testSetPageNumber() throws Exception {

    }

    @Test
    public void testGetPageNumber() throws Exception {
        assertEquals(0, pageParams.getPageNumber());
    }

    @Test
    public void testGetPageSize() throws Exception {
        assertEquals(2, pageParams.getPageSize());
    }
}