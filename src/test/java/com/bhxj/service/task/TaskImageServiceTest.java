package com.bhxj.service.task;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Jean on 7/12/15.
 */
public class TaskImageServiceTest {

    @Test
    public void testDateExtract() throws Exception {
        String file = "[Cer_MTBF_01_Contacts_Run]_[13]_[GUINewContact]_[2015-03-06 15-11-42].jpg";
        int end = file.lastIndexOf("]");
        int start = file.lastIndexOf("[");

        System.out.println(start);
        System.out.println(end);

        String date = file.substring(start+1, end);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

        Date d = simpleDateFormat.parse(date);
        System.out.println(d.toString());
        assertEquals("2015-03-06 15-11-42", date);
    }
}