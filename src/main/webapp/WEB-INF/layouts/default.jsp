<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>滨海云家政 合作商管理系统 2.0</title>
    <meta name="keywords" content="Comcat,neXgenius,Tracing" />
    <meta name="description" content="Comcat,neXgenius,Tracing" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- basic styles -->
    <link href="${ctx}/static/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/font-awesome.min.css" />

    <!--[if IE 7]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/font-awesome-ie7.min.css" />
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="${ctx}/static/assets/css/googleapis.css" />
    <!-- ace styles -->

    <link rel="stylesheet" href="${ctx}/static/assets/css/ace.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-skins.min.css" />

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="${ctx}/static/assets/css/ace-ie.min.css" />
    <![endif]

    <!-- ace settings handler -->
    <script src="${ctx}/static/assets/js/ace-extra.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="${ctx}/static/assets/js/html5shiv.js"></script>
    <script src="${ctx}/static/assets/js/respond.min.js"></script>
    <![endif]-->

    <!--[if !IE]> -->
    <script src="${ctx}/static/assets/js/jQueryNotSupportIE.js"></script>
    <!-- <![endif]-->

    <!--[if IE]>
    <script src="${ctx}/static/assets/js/jQuerySupportIE.js"></script>
    <![endif]-->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='${ctx}/static/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='${ctx}/static/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='${ctx}/static/assets/js/jquery.mobile.custom.min.js'>"+"<"+"script>");
    </script>
    <script src="${ctx}/static/assets/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/assets/js/typeahead-bs2.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
    <script src="${ctx}/static/assets/js/excanvas.min.js"></script>
    <![endif]-->

    <script src="${ctx}/static/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="${ctx}/static/assets/js/bootbox.min.js"></script>

    <%--<script src="${ctx}/static/assets/js/jquery.slimscroll.min.js"></script> --%>

    <!-- ace scripts -->
    <script src="${ctx}/static/assets/js/ace-elements.min.js"></script>
    <script src="${ctx}/static/assets/js/ace.min.js"></script>
    <script src="${ctx}/static/assets/js/chosen.jquery.min.js"></script>
    <script src="${ctx}/static/assets/js/jquery-html5Validate.js"></script>

    <%--<script src="${ctx}/static/assets/js/ace-extra.min.js"></script>--%>

    <!-- inline scripts related to this page -->
    <script src="${ctx}/static/assets/js/menu-marker.js"></script>
</head>

<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <%@ include file="/WEB-INF/layouts/header.jsp" %>
</div>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

    <div class="main-container-inner">
        <%@ include file="/WEB-INF/layouts/left.jsp" %>
        <sitemesh:body/>
    </div><!-- /.main-container-inner -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->





<script type="text/javascript">
    jQuery(function($) {
        $('[data-rel=tooltip]').tooltip();

        //Android's efault browser somehow is confused when tapping on label which will lead to dragging the task
        //so disable dragging when clicking on label
        var agent = navigator.userAgent.toLowerCase();
        if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
            $('#tasks').on('touchstart', function(e){
                var li = $(e.target).closest('#tasks li');
                if(li.length == 0)return;
                var label = li.find('label.inline').get(0);
                if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
            });

        $('#tasks').sortable({
                    opacity:0.8,
                    revert:true,
                    forceHelperSize:true,
                    placeholder: 'draggable-placeholder',
                    forcePlaceholderSize:true,
                    tolerance:'pointer',
                    stop: function( event, ui ) {//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                        $(ui.item).css('z-index', 'auto');
                    }
                });
        $('#tasks').disableSelection();
        $('#tasks input:checkbox').removeAttr('checked').on('click', function(){
            if(this.checked) $(this).closest('li').addClass('selected');
            else $(this).closest('li').removeClass('selected');
        });
        /*
        $("#nav-search .form-search input").on("blur",function(){
            $(this).val($.trim($(this).val()));
        });*/
        $("input").on("blur",function(){
            $(this).val($.trim($(this).val()));
        });
    });
    /** 提取公共方法 @2015-9-11*/
    function normAjax(url,asyncFlag, fn){
        url = encodeURI(url);
        $.ajax({
            contentType: 'json',
            dataType: 'json',
            url:  url,
            async: asyncFlag,
            method: 'GET',
            success: function (data) {
                fn(data);
            },
            fail: function (err) {
                return null;
            }
        });
    }
</script>
</body>
</html>

