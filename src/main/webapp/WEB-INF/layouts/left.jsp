<%--
  Created by IntelliJ IDEA.
  User: LawrenceQi
  Date: 15/7/14
  Time: PM6:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>--%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<style>
    .tooltip-info + .tooltip.bottom .tooltip-arrow {
        border-bottom-color: #d0d8e0
    }

    .tooltip-info + .tooltip > .tooltip-inner {
        background-color: #d0d8e0;
        color: #000;
        border-radius: 0
    }
</style>
<%
    if (session.getAttribute("supplier") != null) {

%>
<div class="sidebar" id="sidebar">

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success tooltip-info" data-rel="tooltip" data-placement="bottom" title
               data-original-title="添加商品" href="${ctx}/systems/goods/create">
                <i class="icon-picture"></i>
            </a>

            <a class="btn btn-info tooltip-info" data-rel="tooltip" data-placement="bottom" title
               data-original-title="文字识别配置" href="${ctx}/ocr">
                <i class="icon-font"></i>
            </a>

            <a class="btn btn-warning tooltip-info" data-rel="tooltip" data-placement="bottom" title
               data-original-title="标签配置" href="${ctx}/code/create/-1">
                <i class="icon-tag"></i>
            </a>

            <a class="btn btn-danger tooltip-info" data-rel="tooltip" data-placement="bottom" title
               data-original-title="问题分类" href="${ctx}/problem">
                <i class="icon-pushpin"></i>
            </a>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div>
    <!-- #sidebar-shortcuts -->


    <ul class="nav nav-list">
        <!-- task monitor-->
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> 查看订单 </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li id="menu_monitor_device">
                    <a href="${ctx}/analyze/terminal">
                        <i class="icon-eye-open"></i>
                        待处理订单
                    </a>
                </li>
                <li id="menu_monitor_task">
                    <a href="${ctx}/analyze">
                        <i class="icon-eye-open"></i>
                        进行中订单
                    </a>
                </li>
                <li id="menu_error_image">
                    <a href="${ctx}/error-image">
                        <%--<a href="#">--%>
                        <i class="icon-picture"></i>
                        <span class="menu-text"> 已完成订单 </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-calendar"></i>
                <span class="menu-text"> 工单管理 </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li id="menu_report_device">
                    <a href="${ctx}/report/reportByCode/default">
                        <i class="icon-eye-open"></i>
                        工单添加
                    </a>
                </li>

                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        工单日历
                    </a>
                </li>
            </ul>
        </li>
        <!-- config -->
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-cog"></i>
                <span class="menu-text"> 查看账款 </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li id="menu_conf_problem">
                    <a href="${ctx}/problem">
                        <i class="icon-plus"></i>
                        当日账款
                    </a>
                </li>

                <li id="menu_report_device">
                    <a href="${ctx}/report/reportByCode/default">
                        <i class="icon-eye-open"></i>
                        周账款
                    </a>
                </li>

                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        月账款
                    </a>
                </li>

            </ul>
        </li>

        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-calendar"></i>
                <span class="menu-text"> 员工管理 </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li id="menu_report_device">
                    <a href="${ctx}/report/reportByCode/default">
                        <i class="icon-eye-open"></i>
                        员工添加
                    </a>
                </li>

                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        员工维护
                    </a>
                </li>

                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        员工工资
                    </a>
                </li>

            </ul>
        </li>

        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-calendar"></i>
                <span class="menu-text"> 客户管理 </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li id="menu_report_device">
                    <a href="${ctx}/report/reportByCode/default">
                        <i class="icon-eye-open"></i>
                        客户添加
                    </a>
                </li>

                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        客户维护
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-calendar"></i>
                <span class="menu-text"> 系统管理 </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">

                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-edit"></i>
                        <span class="menu-text"> 合作商管理 </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <li id="menu_conf_image_match">
                            <a href="${ctx}/image-match">
                                <i class="icon-plus"></i>添加合作商
                            </a>
                        </li>
                        <li id="menu_conf_ocr">
                            <a href="${ctx}/ocr">
                                <i class="icon-plus"></i>合作商维护
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-edit"></i>
                        <span class="menu-text"> 商品管理 </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <li id="menu_add_goods">
                            <a href="${ctx}/systems/goods/create">
                                <i class="icon-plus"></i>添加商品
                            </a>
                        </li>
                        <li id="menu_conf_ocr">
                            <a href="${ctx}/systems/goods">
                                <i class="icon-plus"></i>商品维护
                            </a>
                        </li>
                    </ul>
                <li id="menu_report_tag">
                    <a href="${ctx}/report/reportByPcode/chip">
                        <i class="icon-eye-open"></i>
                        账务管理
                    </a>
                </li>
        </li>

    </ul>
    </li>


    </ul>
    <!-- /.nav-list -->
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left"
           data-icon2="icon-double-angle-right"></i>
    </div>


</div>


<%
    }
%>
