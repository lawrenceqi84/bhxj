<%--<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter" %>--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>--%>
<%--<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>--%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<head>
    <meta charset="utf-8"/>
    <title>滨海云家政合作商管理系统</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


</head>
<div class="main-container login-layout">
    <div class="main-content">
        <div class="row">
            <%--<%--%>
                <%--if (session.getAttribute("supplier") != null) {--%>

            <%--%>--%>
            <%--<div class="col-sm-10 col-sm-offset-1" style="width: 100%;">--%>
                <%--<%}%>--%>
                <%--<shiro:guest>--%>
                <%--<div class="col-sm-12">--%>
                <%--</shiro:guest>--%>
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i class="orange"></i>
                            <span class="blue"></span>
                            <span class="blue">滨海云家政合作商系统</span>
                        </h1>
                        <h4 class="blue">&copy; 天津迅洁物业管理有限公司</h4>
                    </div>

                    <div class="space-6"></div>
                    <div class="position-relative">
                        <form id="loginForm" action="${ctx}/systems/login" method="post" class="form-horizontal">
                            <c:if test="${error!=null}">
                            <div id="message" class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                    ${error}
                            </div>
                            </c:if>
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="icon-coffee blue"></i>
                                            请输入用户名和密码
                                        </h4>

                                        <div class="space-6"></div>
                                        <form>
                                            <fieldset>
                                                <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control"
                                                                   placeholder="用户名" name="username"
                                                                   id="username"/>
															<i class="icon-user"></i>
														</span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" class="form-control"
                                                               placeholder="密码" id="password"
                                                               name="password"/>
                                                        <i class="icon-lock"></i>
                                                    </span>
                                                </label>

                                                <div class="space"></div>

                                                <div class="clearfix">
                                                    <%--<label class="inline">--%>
                                                        <%--<input type="checkbox" class="ace" name="rememberMe"--%>
                                                               <%--id="rememberMe"/>--%>
                                                        <%--<span class="lbl"> 记住用户名</span>--%>
                                                    <%--</label>--%>

                                                    <button type="submit" id="submit_btn"
                                                            class="width-35 pull-right btn btn-sm btn-primary">
                                                        <i class="icon-key"></i>
                                                        登录
                                                    </button>
                                                </div>
                                                <div class="space-4"></div>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <!-- /widget-main -->
                                </div>
                                <!-- /widget-body -->
                            </div>
                            <!-- /login-box -->
                            <form/>




                    </div>
                    <!-- /position-relative -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</div>
<!-- /.main-container -->

<script type="text/javascript">
    function show_box(id) {
        jQuery('.widget-box.visible').removeClass('visible');
        jQuery('#' + id).addClass('visible');
    }
</script>