<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<div class="main-content">
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="icon-home home-icon"></i>
                首页
            </li>
            <%--<li class="active">控制台</li>--%>
        </ul>
        <!-- .breadcrumb -->
    </div>

    <div class="page-content">
        <!-- /.page-header -->
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="space-6"></div>

                    <div class="col-sm-6 infobox-container" id="general-info">
                        <div class="infobox infobox-orange2  ">
                            <div class="infobox-icon">
                                <i class="icon-th"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${unresolved}2</span>

                                <div class="infobox-content">待处理订单</div>
                            </div>
                        </div>
                        <div class="infobox infobox-red  ">
                            <div class="infobox-icon">
                                <i class="icon-bug"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${resolved}12</span>

                                <div class="infobox-content">当月订单</div>
                            </div>
                        </div>
                        <div class="infobox infobox-blue  ">
                            <div class="infobox-icon">
                                <i class="icon-android bigger-150"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${testingDevice}1000</span>

                                <div class="infobox-content">未结算欠款</div>
                            </div>
                        </div>

                        <div class="infobox infobox-pink  ">
                            <div class="infobox-icon">
                                <i class="icon-list"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${testingTask}100000</span>

                                <div class="infobox-content">当月收入</div>
                            </div>
                        </div>

                        <div class="infobox infobox-orange  ">
                            <div class="infobox-icon">
                                <i class="icon-picture"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${countImage}5000</span>

                                <div class="infobox-content">
                                    <a href="${ctx}/error-image">
                                        月工资
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="infobox infobox-orange  ">
                            <div class="infobox-icon">
                                <i class="icon-picture"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">${countImage}300000</span>

                                <div class="infobox-content">
                                    <a href="${ctx}/error-image">
                                        账户余额
                                    </a>
                                </div>
                            </div>
                        </div>
                        <%--<div--%>
                        <%--<c:if test="${used >= 90}">--%>
                        <%--class="infobox infobox-red" style="background-color: #ffd5d3"--%>
                        <%--</c:if>--%>

                        <%--<c:if test="${used < 90}">--%>
                        <%--class="infobox infobox-blue"--%>
                        <%--</c:if>--%>
                        <%-->--%>
                        <%--<div class="infobox-progress">--%>
                        <%--<div class="easy-pie-chart percentage" data-percent="${used}" data-size="46">--%>
                        <%--<span class="percent">${used}</span>%--%>
                        <%--</div>--%>
                        <%--</div>--%>

                        <%--<div class="infobox-data">--%>
                        <%--<span class="infobox-text">磁盘空间</span>--%>

                        <%--<div class="infobox-content">--%>
                        <%--<span class="bigger-110">~</span>--%>
                        <%--剩余${free}GB--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>

                    <div class="vspace-sm"></div>

                    <div class="col-sm-5">
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                    <i class="icon-signal"></i>
                                    客户区域分布
                                </h5>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="piechart-placeholder"></div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <!-- /widget-main -->
                            </div>
                            <!-- /widget-body -->
                        </div>
                        <!-- /widget-box -->
                    </div>
                    <!-- /span -->
                </div>
                <!-- /row -->

                <div class="hr hr-dotted"></div>

                <div class="row">
                    <div class="col-sm-5" style="width:100%">
                        <div class="widget-box transparent">
                            <div class="widget-header widget-header-flat">
                                <h4 class="lighter">
                                    <i class="icon-warning-sign blue"></i>
                                    待处理订单
                                </h4>

                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>


                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <table class="table table-bordered table-striped" id="problems-table">
                                        <thead class="thin-border-bottom">
                                        <tr>
                                            <th>
                                                <i class="icon-caret-right blue"></i>
                                                操作
                                            </th>
                                            <th>
                                                <i class="icon-caret-right blue"></i>
                                                订单编号
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                时间
                                            </th>
                                            <th>
                                                <i class="icon-caret-right blue"></i>
                                                服务类型
                                            </th>

                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                要求人数
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                要求时长
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                价格
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                付款状态
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                客户地址
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                客户姓名
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                联系方式
                                            </th>
                                            <th class="hidden-480">
                                                <i class="icon-caret-right blue"></i>
                                                客户级别
                                            </th>

                                        </tr>
                                        </thead>

                                        <tbody>
                                        <c:forEach items="${reportByCode}" var="reportByCode">
                                            <tr>
                                                <td>${reportByCode.name}</td>

                                                <td class="center">
                                                    <c:if test="${reportByCode.network!='0'}">
                                                        <span class="badge badge-danger  ">${reportByCode.network}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.network=='0'}">
                                                        <b class="green">${reportByCode.network} </b>
                                                    </c:if>

                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.restart!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.restart}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.restart==0}">
                                                        <b class="green">${reportByCode.restart} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.app!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.app}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.app==0}">
                                                        <b class="green">${reportByCode.app} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.crash!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.crash}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.crash==0}">
                                                        <b class="green">${reportByCode.crash} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.sim!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.sim}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.sim==0}">
                                                        <b class="green">${reportByCode.sim} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.sd!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.sd}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.sd==0}">
                                                        <b class="green">${reportByCode.sd} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.screen!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.screen}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.screen==0}">
                                                        <b class="green">${reportByCode.screen} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.battery!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.battery}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.battery==0}">
                                                        <b class="green">${reportByCode.battery} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.connect!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.connect}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.connect==0}">
                                                        <b class="green">${reportByCode.connect} </b>
                                                    </c:if>
                                                </td>
                                                <td class="center">
                                                    <c:if test="${reportByCode.others!=0}">
                                                        <span class="badge badge-danger ">${reportByCode.others}</span>
                                                    </c:if>
                                                    <c:if test="${reportByCode.others==0}">
                                                        <b class="green">${reportByCode.others} </b>
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /widget-main -->
                            </div>
                            <!-- /widget-body -->
                        </div>
                        <!-- /widget-box -->
                    </div>
                </div>
                <div class="hr hr32 hr-dotted"></div>
                <!-- PAGE CONTENT ENDS -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
<!-- /.main-content -->
<script src="${ctx}/static/assets/js/flot/jquery.flot.min.js"></script>
<script src="${ctx}/static/assets/js/flot/jquery.flot.pie.min.js"></script>
<script src="${ctx}/static/assets/js/flot/jquery.flot.resize.min.js"></script>
<script src="${ctx}/static/assets/js/jquery.easy-pie-chart.min.js"></script>
<%--<script src="${ctx}/static/assets/js/jquery.sparkline.min.js"></script>--%>
<script>
    jQuery(function ($) {
        $('.easy-pie-chart.percentage').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
            var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
            var size = parseInt($(this).data('size')) || 50;
            $(this).easyPieChart({
                barColor: barColor,
                trackColor: trackColor,
                scaleColor: false,
                lineCap: 'butt',
                lineWidth: parseInt(size / 10),
                animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
                size: size
            });
        });
        $('.sparkline').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
            $(this).sparkline('html', {
                tagValuesAttribute: 'data-values',
                type: 'bar',
                barColor: barColor,
                chartRangeMin: $(this).data('min') || 0
            });
        });

        var placeholder = $('#piechart-placeholder').css({'width': '90%', 'min-height': '230px'});
        var data = [
            ${problemPie}
        ];

        function drawPieChart(placeholder, data, position) {
            $.plot(placeholder, data, {
                series: {
                    pie: {
                        show: true,
                        tilt: 0.8,
                        highlight: {
                            opacity: 0.25
                        },
                        stroke: {
                            color: '#fff',
                            width: 2
                        },
                        startAngle: 2
                    }
                },
                legend: {
                    show: true,
                    position: position || "ne",
                    labelBoxBorderColor: null,
                    margin: [-30, 15]
                }
                ,
                grid: {
                    hoverable: true,
                    clickable: true
                }
            })
        }

        drawPieChart(placeholder, data);

        placeholder.data('chart', data);
        placeholder.data('draw', drawPieChart);

        var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
        var previousPoint = null;

        placeholder.on('plothover', function (event, pos, item) {
            if (item) {
                if (previousPoint != item.seriesIndex) {
                    previousPoint = item.seriesIndex;
                    var tip = item.series['label'] + " : " + item.series['percent'] + '%';
                    $tooltip.show().children(0).text(tip);
                }
                $tooltip.css({top: pos.pageY + 10, left: pos.pageX + 10});
            } else {
                $tooltip.hide();
                previousPoint = null;
            }
        });
    });
    setTimeout(function () {
        window.location = '${ctx}/index';
    }, 60000);
</script>


<style>
    .infobox {
        padding: 8px 3px 6px 29px;
    }

    .infobox > .infobox-data {
        padding-left: 28px;
    }

    #problems-table {
        border: 1px solid #ddd;
        border-top: 0;
    }

    #general-info {
        margin-top: 10px;
    }

    .infobox a {
        text-decoration: none;
    }
</style>
