<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>


<div class="main-content">

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="icon-home home-icon"></i>
                <a href="${ctx}/systems/">首页</a>
            </li>
            <li class="active">商品列表</li>
        </ul>
        <!-- .breadcrumb -->
        <div class="nav-search" id="nav-search">
            <form class="form-search" action="${ctx}/systems/goods" method="get">
                <label>任务名称：</label> <span class="input-icon">
									<input name="search_LIKE_title" value="${param.search_LIKE_title}"
                                           type="text" placeholder="搜索 ..." class="nav-search-input"/>
									<i class="icon-search nav-search-icon"></i>
								</span>
                <button type="submit" class="btn btn-minier btn-purple">搜索</button>
            </form>



        </div>

        <!-- #nav-search -->
    </div>

    <%--<div class="page-content">--%>
    <%--<div class="page-header">--%>
    <%--<h1>--%>
    <%--控制台--%>
    <%--<small>--%>
    <%--<i class="icon-double-angle-right"></i>--%>
    <%--查看--%>
    <%--</small>--%>
    <%--</h1>--%>
    <%--</div><!-- /.page-header -->--%>

    <div class="page-content ">
        <c:if test="${not empty message}">
            <div id="message" class="alert alert-success">
                <button data-dismiss="alert" class="close">×</button>
                    ${message}</div>
        </c:if>
        <div class="page-header">
            <h1>
                商品
                <small>
                    <i class="icon-double-angle-right"></i>
                    商品查看
                </small>
                <a href="${ctx}/systems/goods/create">
                    <button class="btn btn-sm btn-primary" type="button" style="float: right">
                        <i class="icon-edit bigger-110"></i>
                        新建商品
                    </button>
                </a>

            </h1>
        </div>
        <div id="goods" class="dataTables_wrapper" role="grid">
            <tags:sort/>

            <table id="sample-table-2"
                   class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th class="center">
                        <label>
                            <input type="checkbox" class="cb_grid-table"/>
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th class="hidden-480">管理</th>
                    <th>商品名称</th>
                    <th>价格</th>
                    <th class="bigger-110">销量</th>
                    <th class="center">状态</th>

                    <th>
                        <i class="icon-time  hidden-480"></i>
                        开始时间
                    </th>
                    <th>
                        <i class="icon-time bigger-110 hidden-480"></i>
                        结束时间
                    </th>


                </tr>
                </thead>

                <tbody>
                <c:forEach items="${goodsList.content}" var="goods">
                    <tr>
                        <td class="center">
                            <label>
                                <input type="checkbox" class="cb_grid-table"/>
                                <span class="lbl"></span>
                            </label>
                        </td>
                        <td>
                            <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                <a class="green" href="${ctx}/systems/goods/update/${goods.id}">
                                    <i class="icon-pencil bigger-130"></i>
                                </a>
                                <a class="red" href="#" onclick="deleteConfirm('${goods.id}')">
                                    <i class="icon-trash bigger-130"></i>
                                </a>
                            </div>
                        </td>
                        <td>
                                ${goods.title}
                        </td>
                        <td>
                                ${goods.currentPrice}
                        </td>

                        <td>
                                ${goods.saleNum}
                        </td>

                        <td class="center">
                            <c:if test="${goods.status == 0}">
                                <i class="icon-coffee blue bigger"></i>
                            </c:if>
                            <c:if test="${goods.status == 1}">
                                <%--<i class="icon-spinner icon-spin orange bigger"></i>--%>
                                <i class="icon-flag green bigger"></i>
                            </c:if>
                            <c:if test="${goods.status == 2}">

                            </c:if>
                        </td>

                        <td>${goods.saleTime}</td>

                        <td class="hidden-480">
                                ${goods.endTime}
                        </td>


                    </tr>
                </c:forEach>

                </tbody>
            </table>
            <tags:pagination page="${goodsList}" paginationSize="10"/>
        </div>
    </div>
</div>
<!-- /.page-content -->
</div><!-- /.main-content -->

<script type="text/javascript">
    $(function () {
        $('#goods th input:checkbox').on('click', function () {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function () {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

        });
    });
    function deleteConfirm(id) {
        bootbox.confirm("您确定删除该商品吗?", function (result) {
            if (result) {
                document.location.href = '${ctx}/systems/goods/delete/' + id;
            }
        });
    }

</script>













