<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>


<div class="main-content">

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try {
                ace.settings.check('breadcrumbs', 'fixed')
            } catch (e) {
            }

        </script>

        <ul class="breadcrumb">
            <li>
                <i class="icon-home home-icon"></i>
                <a href="${ctx}/systems">首页</a>
            </li>
            <li class="active">
                <a href="${ctx}/goods">商品列表</a>
            </li>
            <li class="active">
                <c:if test="${action == 'update'}">编辑商品</c:if>
                <c:if test="${action == 'create'}">创建商品</c:if>
            </li>
        </ul>
        <!-- .breadcrumb -->


        <!-- #nav-search -->
    </div>


    <div class="page-content ">
        <c:if test="${not empty message}">
            <div id="message" class="alert alert-success">
                <button data-dismiss="alert" class="close">×</button>
                    ${message}</div>
        </c:if>
        <%--<div class="page-header">
            <h1>
                Edit goods
                <small>
                    <i class="icon-double-angle-right"></i>
                    手动编辑任务数据
                </small>
            </h1>
        </div>--%>
        <div class="row">
            <div class="col-xs-12">
                <div class="dataTables_wrapper" role="grid">
                    <form id="inputForm" class="form-horizontal" role="form" action="${ctx}/systems/goods/${action}"  method="post">

                        <input type="hidden" name="id" id="id" value="${goods.id}"/>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label no-padding-right">商品名:</label>

                            <div class="col-sm-9">
                                <input type="text" id="title" name="title" value="${goods.title}"
                                       required
                                       placeholder="商品名 [必填 5~500位]" class="required col-xs-10 col-sm-5" minlength="3"
                                       maxlength="500"/>
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label for="title_id" class="col-sm-3 control-label no-padding-right">商品标记ID:</label>

                            <div class="col-sm-9">
                                <input type="text" id="title_id" name="titleId" value="${goods.titleId}"
                                       required
                                       placeholder="商品标记ID 用于标记商品 用于查询销量等 [必填 5~50位]"
                                       class="required col-xs-10 col-sm-5" minlength="5"
                                       maxlength="50"/>

                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label for="description" class="col-sm-3 control-label no-padding-right">商品描述:</label>

                            <div class="col-sm-9">
                                <textarea id="description" name="description"        style='margin-left: 4px;'
                                          required
                                          placeholder="描述 [必填 5~5000位]" class="required col-xs-10 col-sm-5"
                                          minlength="5"
                                          maxlength="5000">${goods.description}</textarea>

                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="goods_style"
                                   class="col-sm-3 control-label no-padding-right">商品类型:</label>

                            <div class="col-sm-9">
                                <select class="form-control" id="goods_style" name="goodsStyle" style="width: 41.6666%"   value="${goods.goodsStyle}"
                                        required>
                                    <option value="0" <c:if test="${goods.goodsStyle==0}">selected</c:if>>包月</option>
                                    <option value="1" <c:if test="${goods.goodsStyle==1}">selected</c:if>>小时工</option>
                                    <option value="2" <c:if test="${goods.goodsStyle==2}">selected</c:if>>单次卫生</option>
                                </select>

                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="market_price" class="col-sm-3 control-label no-padding-right">市场价:</label>

                            <div class="col-sm-9">
                                <input id="market_price" name="marketPrice" value="${goods.marketPrice}" required
                                       placeholder="显示出的市场价格 [必填 0~10位] （数字验证未实现）" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="current_price" class="col-sm-3 control-label no-padding-right">现价格:</label>

                            <div class="col-sm-9">
                                <input id="current_price" name="currentPrice" value="${goods.currentPrice}" required
                                       placeholder="显示出的物品价格 [必填 0~10位] （数字验证未实现）" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text" reg="/^[a-zA-Z\u4e00-\u9fa5]+$/"/>

                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="save_price" class="col-sm-3 control-label no-padding-right">节省价格:</label>

                            <div class="col-sm-9">
                                <input id="save_price" name="savePrice" value="${goods.savePrice}" required
                                       placeholder="显示出节省的价格 [必填 0~10位] （数字验证未实现）" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>


                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="discount" class="col-sm-3 control-label no-padding-right">折扣率:</label>

                            <div class="col-sm-9">
                                <input id="discount" name="discount" value="${goods.discount}" required
                                       placeholder="显示出商品折扣率 [必填 0~10位] （数字验证未实现）" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>





                        <%--<div class="space-4"></div>--%>
                        <%--<div class="form-group">--%>
                            <%--<label for="sale_time" class="col-sm-3 control-label no-padding-right">起售时间:</label>--%>

                            <%--<div class="col-sm-9">--%>
                                <%--<input id="sale_time" name="saleTime" value="${goods.saleTime}" required    style='margin-left: 4px;'--%>
                                       <%--class="required col-xs-10 col-sm-5"--%>
                                       <%--type="datetime-local"/>--%>

                            <%--</div>--%>
                        <%--</div>--%>


                        <%--<div class="space-4"></div>--%>
                        <%--<div class="form-group">--%>
                            <%--<label for="end_time" class="col-sm-3 control-label no-padding-right">停售时间:</label>--%>

                            <%--<div class="col-sm-9">--%>
                                <%--<input id="end_time" name="endTime" value="${goods.endTime}" required    style='margin-left: 4px;'--%>
                                       <%--class="required col-xs-10 col-sm-5"--%>
                                       <%--type="datetime-local"/>--%>

                            <%--</div>--%>
                        <%--</div>--%>



                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="sale_num">已销售数量:</label>

                            <div class="col-sm-9">
                                <input style='margin-left: 4px;width: 41.6666%' min="1" max="99999999" type="number"
                                       name="saleNum" id="sale_num" value="${goods.saleNum}" required
                                       placeholder="为任务标记标记测试轮次 [必填1~99999999之间数字]"/>
                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="stock_num">库存数量:</label>

                            <div class="col-sm-9">
                                <input style='margin-left: 4px;width: 41.6666%' min="1" max="99999999" type="number"
                                       name="stockNum" id="stock_num" value="${goods.stockNum}" required
                                       placeholder="为任务标记标记测试轮次 [必填1~99999999之间数字]"/>
                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="remain_num">已销售数量:</label>

                            <div class="col-sm-9">
                                <input style='margin-left: 4px;width: 41.6666%' min="1" max="99999999" type="number"
                                       name="remainNum" id="remain_num" value="${goods.remainNum}" required
                                       placeholder="为任务标记标记测试轮次 [必填1~99999999之间数字]"/>
                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="collect_num">收藏数量:</label>

                            <div class="col-sm-9">
                                <input style='margin-left: 4px;width: 41.6666%' min="1" max="99999999" type="number"
                                       name="collectNum" id="collect_num" value="${goods.collectNum}" required
                                       placeholder="为任务标记标记测试轮次 [必填1~99999999之间数字]"/>
                            </div>
                        </div>




                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="tprice" class="col-sm-3 control-label no-padding-right">邮费:</label>

                            <div class="col-sm-9">
                                <input id="tprice" name="tprice" value="${goods.tprice}" required
                                       placeholder="显示出的邮费 [必填 0~10位] （数字验证未实现）" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="position">商品显示位置:</label>

                            <div class="col-sm-9">
                                <input style='margin-left: 4px;width: 41.6666%' min="1" max="99999999" type="number"
                                       name="position" id="position" value="${goods.position}" required
                                       placeholder="商品显示为0位置排在1前方 [必填1~99999999之间数字]"/>
                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="user_id" class="col-sm-3 control-label no-padding-right">专属客户:</label>

                            <div class="col-sm-9">
                                <input id="user_id" name="userId" value="${goods.userId}"
                                       placeholder="商品专门为某个客户创建" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="address_id" class="col-sm-3 control-label no-padding-right">专属地址:</label>

                            <div class="col-sm-9">
                                <input id="address_id" name="addressId" value="${goods.addressId}"
                                       placeholder="商品专门为某个地址创建" class="required col-xs-10 col-sm-5"
                                       minlength="1" maxlength="10"  type="text"/>

                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="supplier_id"
                                   class="col-sm-3 control-label no-padding-right">合作商:</label>

                            <div class="col-sm-9">
                                <select class="form-control" id="supplier_id" name="supplierId" style="width: 41.6666%"
                                        required>
                                    <option value="0" <c:if test="${goods.supplierId==1}">selected</c:if>>滨海迅洁</option>
                                    <option value="1" <c:if test="${goods.supplierId==2}">selected</c:if>>东方蜜蜂</option>
                                    <option value="2" <c:if test="${goods.supplierId==2}">selected</c:if>>山东单姐</option>
                                    <option value="3" <c:if test="${goods.supplierId==2}">selected</c:if>>河北王凤清</option>
                                </select>

                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="reward-check" class="col-sm-3 control-label no-padding-right">是否为抽奖商品:</label>

                            <div class="col-sm-9">
                                <input hidden name="reward" id="goods-reward" value="${goods.reward}"/>
                                <input id="reward-check"
                                <c:if test="${goods.reward == 1}">
                                       checked
                                </c:if>
                                       type="checkbox" class="ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="delivery-check" class="col-sm-3 control-label no-padding-right">是否为自提商品:</label>

                            <div class="col-sm-9">
                                <input hidden name="delivery" id="goods-delivery" value="${goods.delivery}"/>
                                <input id="delivery-check"
                                <c:if test="${goods.delivery == 1}">
                                       checked
                                </c:if>
                                       type="checkbox" class="ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                            </div>
                        </div>


                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="hot-check" class="col-sm-3 control-label no-padding-right">是否显示为热卖商品:</label>

                            <div class="col-sm-9">
                                <input hidden name="hot" id="goods-hot" value="${goods.hot}"/>
                                <input id="hot-check"
                                <c:if test="${goods.hot == 1}">
                                       checked
                                </c:if>
                                       type="checkbox" class="ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div class="form-group">
                            <label for="status-check" class="col-sm-3 control-label no-padding-right">是否上线售卖:</label>

                            <div class="col-sm-9">
                                <input hidden name="status" id="goods-status" value="${goods.status}"/>
                                <input id="status-check"
                                <c:if test="${goods.status == 1}">
                                checked
                                </c:if>
                                       type="checkbox" class="ace ace-switch ace-switch-5">
                                <span class="lbl"></span>
                            </div>
                        </div>



                        <%--<div class="space-4"></div>--%>
                        <%--<div class="form-group">--%>
                            <%--<label for="imgUpload" class="col-sm-3 control-label no-padding-right">商品配图:</label>--%>

                            <%--<div class="col-sm-9">--%>
                                <%--<input type="file" id="imgUpload" name="imgUpload"  accept="image/*" single value="${goods.url}" />--%>
                                <%--<input type="hidden" id="url" name="url" value="${goods.url}"/>--%>
                                <%--<!--允许file控件接受的文件类型-->--%>
                                <%--<!--<input type="file" id="imgUpload" name="imgUpload" accept="image/*" multiple/>-->--%>
                                <%--<div id="destination" name="destination" style="width:300px;height:200px;margin-top: 10px;">--%>
                                    <%--<img src="${goods.httpUrl}" style="max-height: 200px;max-width: 300px;border:1px solid #fcf4f9;"/>--%>
                                <%--</div>--%>
                            <%--</div>--%>


                            <%--<div class="col-sm-9">--%>
                                <%--<input type="file" id="imgUpload" name="imgUpload" draggable="true" accept="image/*" single value="${imageMatch.url}" />--%>
                                <%--<input type="hidden" id="url" name="url" value="${imageMatch.url}"/>--%>
                                <%--<!--允许file控件接受的文件类型-->--%>
                                <%--<!--<input type="file" id="imgUpload" name="imgUpload" accept="image/*" multiple/>-->--%>
                                <%--<div id="destination" style="width:300px;height:200px;margin-top: 10px;">--%>
                                    <%--<img src="${imageMatch.httpUrl}" style="max-height: 200px;max-width: 300px;border:1px solid #fcf4f9;"/>--%>
                                <%--</div>--%>
                            <%--</div>--%>


                        <%--</div>--%>
                        <%--<div class="space-4"></div>--%>
                        <%--<div class="form-group">--%>
                            <%--<label for="imgUpload2" class="col-sm-3 control-label no-padding-right">商品配图2:</label>--%>

                            <%--<div class="col-sm-9">--%>
                                <%--&lt;%&ndash;<input type="file" id="imgUpload2" name="imgUpload"  accept="image/*" single value="${goods.url2}" />&ndash;%&gt;--%>
                                <%--<input type="hidden" id="url2" name="url" value="${goods.url2}"/>--%>
                                <%--<!--允许file控件接受的文件类型-->--%>
                              <%--<input type="file" id="imgUpload" name="imgUpload" accept="image/*" multiple/>--%>
                                <%--<div id="destination2" name="destination" style="width:300px;height:200px;margin-top: 10px;">--%>
                                    <%--<img src="${goods.httpUrl2}" style="max-height: 200px;max-width: 300px;border:1px solid #fcf4f9;"/>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</div>--%>

                        <div class="space-4"></div>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-info" id="btn_save">
                                    <i class="icon-ok bigger-110"></i>
                                    保存
                                </button>

                                &nbsp; &nbsp; &nbsp;

                                <button class="btn" type="button" onclick="history.back()">
                                    <i class="icon-undo bigger-110"></i>
                                    返回
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.page-content -->
</div><!-- /.main-content -->

<link rel="stylesheet" href="${ctx}/static/assets/css/chosen.css"/>
<script src="${ctx}/static/assets/js/bootstrap-tag.min.js"></script>
<script type="text/javascript">
    //聚焦第一个输入框
    $("#nx_user_name").focus();

    $(function () {
        $("#status-check").on('click', function () {
            var checked = $(this).is(":checked");
            if (checked) {
                $("#goods-status").val("1");
            } else {
                $("#goods-status").val("0");
            }
        });
        $("#hot-check").on('click', function () {
            var checked = $(this).is(":checked");
            if (checked) {
                $("#goods-hot").val("1");
            } else {
                $("#goods-hot").val("0");
            }
        });
        $("#reward-check").on('click', function () {
            var checked = $(this).is(":checked");
            if (checked) {
                $("#goods-reward").val("1");
            } else {
                $("#goods-reward").val("0");
            }
        });
        $("#delivery-check").on('click', function () {
            var checked = $(this).is(":checked");
            if (checked) {
                $("#goods-delivery").val("1");
            } else {
                $("#goods-delivery").val("0");
            }
        });
        $("#affectAll-check").on('click', function () {
            var checked = $(this).is(":checked");
            if (checked) {
                $("#affectAll").val("1");
            } else {
                $("#affectAll").val("0");
            }
        });
    });
    //为inputForm注册validate函数
    //$("#inputForm").validate();





    jQuery(document).ready(function () {


//        $("#btn_save").on("click",function(){
//            var pattern=/^[a-zA-Z0-9\u4e00-\u9fa5]+$/im;
//            if(!pattern.test(imageMatchName.val())){
//                showAlarmInfo(imageMatchName,"问题名称含有非法字符!");
//                return false;
//            }
//                return false;
         //   $(".form-horizontal").validate();
            <%--var imageMatch = $("#imgUpload");--%>
            <%--if(imageMatch.val() != "") {--%>
                <%--console.info(imageMatch.val());--%>
            <%--}--%>
            <%--if(${action == 'create'}){--%>
                <%--if(imageMatch.val() == null || imageMatch.val() == "") {--%>
                    <%--showAlarmInfo(imageMatch,"请选择特征图!");--%>
                    <%--//return false;--%>
                <%--}--%>
            <%--} else if(${action == 'update'}) {--%>
                <%--if(${imageMatch.url == ''}) {--%>
                    <%--showAlarmInfo(imageMatch,"请选择特征图!");--%>
                    <%--return false;--%>
                <%--}--%>
            <%--}--%>
//            $(".form-horizontal").submit();
//        });
    });
    //******************************************//
    //处理file input加载的图片文件
    $(document).ready(function(e) {
        //判断浏览器是否有FileReader接口
        if(typeof FileReader =='undefined') {
            $("#destination1").css({'background':'none'}).html('亲,您的浏览器还不支持HTML5的FileReader接口,无法使用图片本地预览,请更新浏览器获得最好体验');
            //如果浏览器是ie
            if($.browser.msie===true) {
                //ie6直接用file input的value值本地预览
                if($.browser.version==6) {
                    $("#imgUpload").change(function(event) {
                        //ie6下怎么做图片格式判断?
                        var src = event.target.value;
                        //var src = document.selection.createRange().text;		//选中后 selection对象就产生了 这个对象只适合ie
                        var img = '<img src="'+src+'" width="220px" height="200px" />';
                        $("#destination").empty().append(img);
                    });
                } //ie7,8使用滤镜本地预览
                else if($.browser.version==7 || $.browser.version==8) {
                    $("#imgUpload").change(function(event){
                        $(event.target).select();
                        var src = document.selection.createRange().text;
                        var dom = document.getElementById('destination');
                        //使用滤镜 成功率高
                        dom.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src= src;
                        dom.innerHTML = '';
                        //使用和ie6相同的方式 设置src为绝对路径的方式 有些图片无法显示 效果没有使用滤镜好
                        /*var img = '<img src="'+src+'" width="220px" height="200px" />';
                         $("#destination").empty().append(img);*/
                    });
                }
            } else if($.browser.mozilla===true) { //如果是不支持FileReader接口的低版本firefox 可以用getAsDataURL接口
                $("#imgUpload").change(function(event){
                    //firefox2.0没有event.target.files这个属性 就像ie6那样使用value值 但是firefox2.0不支持绝对路径嵌入图片 放弃firefox2.0
                    //firefox3.0开始具备event.target.files这个属性 并且开始支持getAsDataURL()这个接口 一直到firefox7.0结束 不过以后都可以用HTML5的FileReader接口了
                    if(event.target.files) {
                        //console.log(event.target.files);
                        for(var i=0;i<event.target.files.length;i++) {
                            var img = '<img src="'+event.target.files.item(i).getAsDataURL()+'" width="220px" height="200px"/>';
                            $("#destination").empty().append(img);
                        }
                    } else {
                        //console.log(event.target.value);
                        //$("#imgPreview").attr({'src':event.target.value});
                    }
                });
            }
        } else {
            //多图上传 input file控件里指定multiple属性 e.target是dom类型
            $("#imgUpload").change(function(e) {
                for(var i=0;i<e.target.files.length;i++) {
                    var file = e.target.files.item(i);
                    //允许文件MIME类型 也可以在input标签中指定accept属性
                    //console.log(/^image\/.*$/i.test(file.type));
                    if(!(/^image\/.*$/i.test(file.type))) {
                        continue;
                    }
                    var freader = new FileReader();
                    freader.readAsDataURL(file);
                    freader.onload=function(e) {
                        var img = '<img src="'+e.target.result+'" width="220px" height="200px"/>';
                        $("#destination").empty().append(img);
                    }
                }
            });
        }
        menu_marker("menu_add_goods");
    });

    function showAlarmInfo(obj,alarmMessage){
        obj.testRemind(alarmMessage, {
            size: 10,
            css: {
                padding: "8px 10px",                ``
                borderColor: "#aaa",
                borderRadius: 4,
                boxShadow: "0px 0px 0px rgba(0,0,0,0)",
                background: "#fff url(${ctx}/static/assets/images/chrome-remind.png) no-repeat 8px 8px",
                backgroundColor: "white",
                fontSize: 14,
                textIndent: 22,
                zIndex:1051
            }
        }).get(0).focus();
        obj.focus();
    }

    /**校验名称**/
    function mateName(obj,url,fn){
        normAjax(url, true, function(data){
            if(data.ok) {
                showAlarmInfo(obj,data.msg);
            } else {
                fn();
            }
        });
    }


</script>








