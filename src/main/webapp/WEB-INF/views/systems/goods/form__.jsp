<%--
  Created by IntelliJ IDEA.
  User: LawrenceQi
  Date: 16/1/1
  Time: PM4:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="ms.css">
    <title></title>
</head>
<body>
<div class="demo-phone-scroll">

  <div class="contact-list demo-scroll">

    <input type="text" id="categories_dummy" class="mbsc-control mbsc-control-ev ng-hide" readonly=""
           placeholder=""><select style="display:none;" id="categories" multiple="" ng-model="selectedCategories"
                                  mobiscroll-select="categorySettings"
                                  class="ng-pristine ng-untouched ng-valid mbsc-comp dw-hsel" tabindex="-1">
    <!-- ngRepeat: category in categories -->
    <option ng-repeat="category in categories" mobiscroll-select-option="" class="ng-binding ng-scope"
            value="Accounts">Accounts
    </option>
    <!-- end ngRepeat: category in categories -->
    <option ng-repeat="category in categories" mobiscroll-select-option="" class="ng-binding ng-scope"
            value="Contacts">Contacts
    </option>
    <!-- end ngRepeat: category in categories -->
    <option ng-repeat="category in categories" mobiscroll-select-option="" class="ng-binding ng-scope"
            value="Leads">Leads
    </option>
    <!-- end ngRepeat: category in categories -->
  </select>

    <input type="hidden" id="calendar" ng-model="appointmentDate" mobiscroll-calendar="{
                    theme: theme,
                    context: context1,
                    animate: 'pop',
                    controls: ['date', 'time'],
                    invalid: invalidDates,
                    stepMinute: 30,
                    minDate: now,
                    onSelect: addAppointment
                }" class="ng-pristine ng-untouched ng-valid mbsc-comp" readonly="" value="">

    <div id="appointments" ng-model="appointments" mobiscroll-event-calendar="{
                    theme: theme,
                    layout: 'liquid',
                    display: 'modal',
                    context: context1,
                    animate: 'pop',
                    markedText: true,
                    eventText: 'appt.',
                    eventsText: 'appt.',
                    buttons: [],
                    showDivergentDays: false
                }" class="ng-pristine ng-untouched ng-valid mbsc-comp"></div>

    <div mbsc-enhance="" class="mbsc-comp mbsc-form mbsc-mobiscroll mbsc-ltr" id="mobiscroll1451636799520">
      <div class="mbsc-lv-cont mbsc-lv-mobiscroll mbsc-lv-ic-anim">
        <ul class="mbsc-lv mbsc-lv-dummy"></ul>
        <div class="mbsc-lv-sl-c" data-ref="1">
          <ul class="contact-detail-edit mbsc-lv mbsc-lv-v mbsc-lv-sl-curr">
            <li class="mbsc-lv-item mbsc-lv-back" data-back="14">Back
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
            </li>
            <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec2" data-ref="15">
              <form class="edit-form ng-pristine ng-valid ng-valid-email" novalidate="novalidate"
                    name="form">
                <div class="mbsc-divider">Contact Details</div>
                <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-valid mbsc-control mbsc-control-ev mbsc-active ng-touched"
                                                                                                style=""><span
                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                </label>

                <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"
                                                                                                   style="height: 34px;"></textarea><span
                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                </label>

                <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                </label>

                <div class="mbsc-divider">Phone</div>
                <!-- ngRepeat: phone in currentItems[$index].phones --><label
                      ng-repeat="phone in currentItems[$index].phones"
                      class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                      class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
              </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                      ng-repeat="phone in currentItems[$index].phones"
                      class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                      class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
              </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                <div class="mbsc-divider">Email</div>
                <!-- ngRepeat: email in currentItems[$index].emails --><label
                      ng-repeat="email in currentItems[$index].emails"
                      class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                      class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
              </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                      ng-repeat="email in currentItems[$index].emails"
                      class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                      class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
              </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact Categories</div>

                <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                      ng-repeat="(cat,v) in currentItems[$index].categories"
                      class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                       ng-model="it.categories[cat]"
                                                                       class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                      class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
              </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                      ng-repeat="(cat,v) in currentItems[$index].categories"
                      class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                       ng-model="it.categories[cat]"
                                                                       class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                      class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
              </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                      ng-repeat="(cat,v) in currentItems[$index].categories"
                      class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                       ng-model="it.categories[cat]"
                                                                       class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                      class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
              </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                <div class="contact-detail-save mbsc-padding">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="checkmark"
                          ng-click="updateContacts(currentItems[$index], $index)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                  </button>
                </div>
              </form>
            </li>
          </ul>
        </div>
        <ul id="contacts" style="" mobiscroll-listview="contactSettings" mobiscroll-data="contacts"
            class="mbsc-comp mbsc-lv mbsc-lv-root">
          <li data-type="filter" class="ng-scope mbsc-lv-item mbsc-lv-item-enhanced" data-ref="2">Display
            contacts: <em class="ng-binding">All</em></li>
          <!-- ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0190" data-email="angelica.geary@gmail.com"
              data-id="1" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="3">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/f3.png" class="contact-icon mbsc-lv-img" src="Content/img/f3.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Angelica Geary</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0190</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="3">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c1"
                  data-ref="4">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/f3.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/f3.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Angelica Geary</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="4">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec1"
                      data-ref="5">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Angelica Geary"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="6">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="7" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="8">Mobiscroll</li>
              <li data-role="list-divider" data-ref="9" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="10">4650 Sunset Boulevard, Los Angeles, CA 90027
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="11" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0107" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0107</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0108" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0108</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="12" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="angelica.geary@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">angelica.geary@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="angelica.geary@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">angelica.geary@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0190" data-email="arlene.sharman@gmail.com"
              data-id="2" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="13">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/f2.png" class="contact-icon mbsc-lv-img" src="Content/img/f2.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Arlene Sharman</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0190</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="13">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c2"
                  data-ref="14">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/f2.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/f2.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Arlene Sharman</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->

              </li>
              <li data-type="newAppointment" data-name="Arlene Sharman"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="16">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="17" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="18">Mobiscroll</li>
              <li data-role="list-divider" data-ref="19" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="20">200 Hospital Drive, Galax, VA 24333
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="21" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0190" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0190</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0192" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0192</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="22" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="arlene.sharman@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">arlene.sharman@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="arlene.sharman@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">arlene.sharman@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>

            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0193" data-email="barry.lyon@gmail.com" data-id="3"
              ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="23">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/m1.png" class="contact-icon mbsc-lv-img" src="Content/img/m1.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Barry Lyon</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0193</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="23">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c3"
                  data-ref="24">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/m1.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/m1.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Barry Lyon</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="24">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec3"
                      data-ref="25">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Barry Lyon"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="26">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="27" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="28">Mobiscroll</li>
              <li data-role="list-divider" data-ref="29" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="30">950 East Bogard Road, Wasilla, AK 99654
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="31" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0193" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0193</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0194" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0194</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="32" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="barry.lyon@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">barry.lyon@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="barry.lyon@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">barry.lyon@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0147" data-email="carl.hambledon@gmail.com"
              data-id="4" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="33">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/m2.png" class="contact-icon mbsc-lv-img" src="Content/img/m2.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Carl Hambledon</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0147</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="33">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c4"
                  data-ref="34">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/m2.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/m2.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Carl Hambledon</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="34">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec4"
                      data-ref="35">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Carl Hambledon"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="36">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="37" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="38">Mobiscroll</li>
              <li data-role="list-divider" data-ref="39" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="40">2160 South 1st Avenue, Maywood, IL 60153
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="41" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0147" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0147</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0148" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0148</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="42" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="carl.hambledon@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">carl.hambledon@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="carl.hambledon@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">carl.hambledon@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0127" data-email="hortense.tinker@gmail.com"
              data-id="5" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="43">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/f1.png" class="contact-icon mbsc-lv-img" src="Content/img/f1.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Hortense Tinker</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0127</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="43">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c5"
                  data-ref="44">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/f1.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/f1.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Hortense Tinker</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="44">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec5"
                      data-ref="45">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Hortense Tinker"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="46">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="47" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="48">Mobiscroll</li>
              <li data-role="list-divider" data-ref="49" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="50">630 Medical Drive, Bountiful, UT 84010
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="51" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0127" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0127</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0128" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0128</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="52" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="hortense.tinker@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">hortense.tinker@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="hortense.tinker@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">hortense.tinker@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0189" data-email="leilah.gregory@gmail.com"
              data-id="6" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="53">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/f4.png" class="contact-icon mbsc-lv-img" src="Content/img/f4.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Leilah Gregory</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0189</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="53">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c6"
                  data-ref="54">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/f4.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/f4.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Leilah Gregory</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="54">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec6"
                      data-ref="55">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Leilah Gregory"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="56">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="57" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="58">Mobiscroll</li>
              <li data-role="list-divider" data-ref="59" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="60">2105 Forest Avenue, San Jose, CA 95128
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="61" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0189" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0189</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0190" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0190</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="62" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="leilah.gregory@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">leilah.gregory@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="leilah.gregory@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">leilah.gregory@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0120" data-email="lowell.christophers@gmail.com"
              data-id="7" ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="63">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/m4.png" class="contact-icon mbsc-lv-img" src="Content/img/m4.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Lowell Christophers</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0120</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="63">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c7"
                  data-ref="64">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/m4.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/m4.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Lowell
                  Christophers</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="64">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec7"
                      data-ref="65">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Lowell Christophers"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="66">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="67" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="68">Mobiscroll</li>
              <li data-role="list-divider" data-ref="69" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="70">6616 Washington Avenue, Ocean Springs, MS 39564
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="71" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0120" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0120</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0121" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0121</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="72" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="lowell.christophers@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">lowell.christophers@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="lowell.christophers@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">lowell.christophers@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts -->
          <li data-type="contact" data-phone="(202) 555-0176" data-email="noble.blythe@gmail.com" data-id="8"
              ng-show="isInCategory(item.categories)"
              class="mbsc-lv-repeat-animation ng-scope mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced mbsc-lv-parent"
              ng-repeat-start="item in contacts" ng-repeat-end="" mobiscroll-listview-item="1451636799523"
              data-ref="73">
            <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
            <img ng-src="Content/img/m3.png" class="contact-icon mbsc-lv-img" src="Content/img/m3.png">

            <h3 ng-bind="item.name" class="ng-binding mbsc-lv-txt">Noble Blythe</h3>

            <p class="ng-binding mbsc-lv-txt">(202) 555-0176</p>
            <ul class="contact-detail mbsc-lv">
              <li class="mbsc-lv-item mbsc-lv-back" data-back="73">Back
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
              </li>
              <li class="contact-hdr mbsc-lv-item mbsc-lv-img-left mbsc-lv-item-enhanced" data-id="c8"
                  data-ref="74">
                <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
                <img ng-src="Content/img/m3.png" class="contact-icon mbsc-lv-img"
                     src="Content/img/m3.png">

                <h3 class="contact-name ng-binding mbsc-lv-txt" ng-bind="item.name">Noble Blythe</h3>
                <!-- ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Accounts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope ng-hide"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Contacts
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <div class="contact-cat-tag ng-binding ng-scope"
                     ng-repeat="(category, val) in item.categories "
                     ng-show="item.categories[category]">Leads
                </div>
                <!-- end ngRepeat: (category, val) in item.categories -->
                <ul class="contact-detail-edit mbsc-lv">
                  <li class="mbsc-lv-item mbsc-lv-back" data-back="74">Back
                    <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                  </li>
                  <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced" data-id="ec8"
                      data-ref="75">
                    <form class="edit-form ng-pristine ng-valid ng-valid-email"
                          novalidate="novalidate" name="form">
                      <div class="mbsc-divider">Contact Details</div>
                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Name
                                                    </span><span class="mbsc-input-wrap"><input name="Name" type="text"
                                                                                                placeholder="Name"
                                                                                                data-icon="user4"
                                                                                                ng-model="currentItems[$index].name"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input mbsc-textarea"><span
                              class="mbsc-label">
                                                    Address
                                                    </span><span class="mbsc-input-wrap"><textarea placeholder="Address"
                                                                                                   data-icon="location"
                                                                                                   name="address"
                                                                                                   ng-model="currentItems[$index].address"
                                                                                                   class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"></textarea><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>
                      </label>

                      <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    Company
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="Company"
                                                                                                data-icon="office"
                                                                                                name="company"
                                                                                                ng-model="currentItems[$index].company"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                              class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                      </label>

                      <div class="mbsc-divider">Phone</div>
                      <!-- ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones --><label
                            ng-repeat="phone in currentItems[$index].phones"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="Phone"
                                                                                                name="phone"
                                                                                                data-icon="phone"
                                                                                                ng-model="phone.number"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                    </label><!-- end ngRepeat: phone in currentItems[$index].phones -->

                      <div class="mbsc-divider">Email</div>
                      <!-- ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Home
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails --><label
                            ng-repeat="email in currentItems[$index].emails"
                            class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span
                            class="mbsc-label">
                                                    Work
                                                    </span><span class="mbsc-input-wrap"><input type="email"
                                                                                                placeholder="Phone"
                                                                                                name="email"
                                                                                                data-icon="foundation-mail"
                                                                                                ng-model="email.email"
                                                                                                class="ng-pristine ng-untouched ng-valid ng-valid-email mbsc-control mbsc-control-ev"><span
                            class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-foundation-mail"></span></span>
                    </label><!-- end ngRepeat: email in currentItems[$index].emails -->

                      <div ng-init="it = currentItems[$index]" class="mbsc-divider">Contact
                        Categories
                      </div>

                      <!-- ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Accounts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Contacts
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories --><label
                            ng-repeat="(cat,v) in currentItems[$index].categories"
                            class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"
                                                                             ng-model="it.categories[cat]"
                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                            class="mbsc-checkbox-box"></span><span class="mbsc-label">Leads
                                                    </span>
                    </label><!-- end ngRepeat: (cat,v) in currentItems[$index].categories -->

                      <div class="contact-detail-save mbsc-padding">
                        <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                data-icon="checkmark"
                                ng-click="updateContacts(currentItems[$index], $index)"><span
                                class="mbsc-btn-ic mbsc-ic mbsc-ic-checkmark"></span>Save
                        </button>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li data-type="newAppointment" data-name="Noble Blythe"
                  class="contact-app-add mbsc-lv-item mbsc-lv-item-enhanced" data-ref="76">
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          ng-click="selectAppointmentDate(item.name)" data-icon="plus"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-plus"></span>Add appointment
                  </button>
                </div>
                <div class="crm-contact-btn">
                  <button class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                          data-icon="pencil" ng-click="editContacts(item.id)"><span
                          class="mbsc-btn-ic mbsc-ic mbsc-ic-pencil"></span>Edit contact
                  </button>
                </div>
              </li>
              <li data-role="list-divider" data-ref="77" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Company
              </li>
              <li class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced" data-ref="78">Mobiscroll</li>
              <li data-role="list-divider" data-ref="79" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Address
              </li>
              <li data-type="address" data-coord="" data-icon="ion-navigate" data-icon-align="right"
                  class="ng-binding mbsc-lv-item mbsc-lv-item-enhanced mbsc-lv-item-ic-right"
                  data-ref="80">1221 North Washington Street, Livingston, AL 35470
                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-ion-navigate"></div>
              </li>
              <li data-role="list-divider" data-ref="81" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Phone
              </li>
              <!-- ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0176" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0176</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="phone"
                  data-phone="(202) 555-0177" ng-repeat="phone in item.phones">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">(202) 555-0177</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-phone"></div>
              </li>
              <!-- end ngRepeat: phone in item.phones -->
              <li data-role="list-divider" data-ref="82" class="mbsc-lv-item-enhanced mbsc-lv-gr-title">
                Email
              </li>
              <!-- ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="noble.blythe@gmail.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Home</h3>

                <p class="mbsc-lv-txt ng-binding">noble.blythe@gmail.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
              <li class="mbsc-lv-item mbsc-lv-item-ic-right ng-scope" data-type="email"
                  data-email="noble.blythe@mobiscroll.com" ng-repeat="email in item.emails">
                <h3 class="mbsc-lv-txt ng-binding">Work</h3>

                <p class="mbsc-lv-txt ng-binding">noble.blythe@mobiscroll.com</p>

                <div class="mbsc-lv-item-ic mbsc-ic mbsc-ic-foundation-mail"></div>
              </li>
              <!-- end ngRepeat: email in item.emails -->
            </ul>
            <div style="display:none;" class="ng-scope mbsc-lv-parent">
              <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-right5"></div>
              <ul class="mbsc-lv">
                <li class="mbsc-lv-item mbsc-lv-back">Back
                  <div class="mbsc-lv-arr mbsc-lv-ic mbsc-ic mbsc-ic-arrow-left5"></div>
                </li>
                <!-- ngRepeat: item in item.children --></ul>
            </div>
          </li>
          <!-- end ngRepeat: item in contacts --></ul>
      </div>
    </div>
  </div>

</div>
</body>
</html>
