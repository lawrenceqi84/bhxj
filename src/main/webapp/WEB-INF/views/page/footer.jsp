<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<div data-role="footer" data-position="fixed" data-theme="a"  >
	<!-- data-tap-toggle="false" -->
	<div data-role="navbar" data-iconpos="right">
		<ul>
			<li><a href="javascript:document.location.href='${ctx}/jqm/index.jsp'" data-prefetch="true"
				data-transition="flip" data-icon="home">首页</a></li>
			<li><a href="javascript:document.location.href='${ctx}/jqm/standard.jsp'" data-prefetch="true"
				data-transition="flip" data-icon="info">服务标准</a></li>
			<li><a href="javascript:document.location.href='${ctx}/jqm/my.jsp'" data-prefetch="true"
				data-transition="flip" data-icon="user">个人中心</a></li>
		</ul>
	</div>

</div>


