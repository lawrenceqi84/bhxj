<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />


<div data-role="header" class="jqm-header" data-position="fixed" data-tap-toggle="false"
	data-theme="b" ><%--
	data-tap-toggle="false"
	--%><h2>
		<img src="${ctx}/static/images/bhxj-logo.png" alt="滨海迅洁"
			style='margin-right:5px;margin-top:5px;'>
	</h2>
	<p>
		<!-- <span class="jqm-version"></span> -->
	</p>
	<a href="javascript:document.location.href='${ctx}/jqm/index.jsp'"
		class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-home ui-nodisc-icon ui-alt-icon ui-btn-left"></a>
	<a href="tel:4006170503"
		class=" ui-btn ui-btn-icon-notext ui-corner-all ui-icon-phone ui-nodisc-icon ui-alt-icon ui-btn-right"></a>
	<!-- ✆ -->
</div>
