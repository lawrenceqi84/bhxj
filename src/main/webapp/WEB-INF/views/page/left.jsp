
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />


<div data-role="panel" class="jqm-navmenu-panel" data-position="left"
	data-display="overlay" data-theme="a">
	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
		<li data-filtertext="demos homepage" data-icon="home"><a
			href=".././">Home</a></li>
		<li data-filtertext="introduction overview getting started"><a
			href="../intro/" data-ajax="false">Introduction</a></li>
		<li data-filtertext="form datepicker widget date input"><a
			href="../datepicker/" data-ajax="false">Datepicker</a></li>
		<li data-role="collapsible" data-enhanced="true"
			data-collapsed-icon="carat-d" data-expanded-icon="carat-u"
			data-iconpos="right" data-inset="false"
			class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
			<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
				<a href="#"
					class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
					Events<span class="ui-collapsible-heading-status"> click to
						expand contents</span> </a>
			</h3>
			<div
				class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed"
				aria-hidden="true">
				<ul>
					<li
						data-filtertext="swipe to delete list items listviews swipe events"><a
						href="../swipe-list/" data-ajax="false">Swipe list items</a></li>
					<li
						data-filtertext="swipe to navigate swipe page navigation swipe events"><a
						href="../swipe-page/" data-ajax="false">Swipe page navigation</a>
					</li>
				</ul>
			</div>
		</li>




	</ul>
</div>
