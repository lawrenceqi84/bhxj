<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%response.setStatus(200);%>

<!DOCTYPE html>
<html>
<head>
    <title>404 - 页面不存在</title>
</head>

<body>
<h2>404 - 页面不存在.</h2>

<p><a href="<c:url value="/systems/login"/>">返回首页</a></p>
<p><input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/></p>

</body>
</html>