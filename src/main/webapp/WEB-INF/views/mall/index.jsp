<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>滨海迅洁</title>
    <meta name="viewport"
          content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta http-equiv="Expires" CONTENT="0">
    <meta http-equiv="Cache-Control" CONTENT="no-cache">
    <meta http-equiv="Cache-Control" CONTENT="no-store">
    <!--css-->

    <link rel="stylesheet"
          href="${ctx}/static/styles/themes/default/jquery.mobile-1.4.5.min.css?type=123">
    <link rel="stylesheet"
          href="${ctx}/static/styles/themes/default/jqm-demos.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/static/styles/style.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/static/styles/t.css">
    <link type="text/css" rel="stylesheet" href="${ctx}/static/styles/page.css">

    <style>
    </style>
    <!--js-->
    <%
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
    %>

</head>
<body>
<div data-role="page" class="jqm-demos jqm-home" data-title="首页">
    <div data-role="header" class="jqm-header" data-position="fixed"
         style='height:50px' data-theme="b" data-tap-toggle="false">
        <h2 style='width:140px;margin-left: 31%;margin-top: 10px;padding:0'>
            <img src="${ctx}/static/images/bhxj-logo.png" alt="滨海迅洁"
                 style='width:140px;height:33px'>
        </h2>

        <p>
            <!-- <span class="jqm-version"></span> -->
        </p>
        <a href="javascript:document.location.href='${ctx}/static/index.jsp'"
           class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-home ui-nodisc-icon ui-alt-icon ui-btn-left">主页</a>
        <a href="tel:4006170503"
           class=" ui-btn ui-btn-icon-notext ui-corner-all ui-icon-phone ui-nodisc-icon ui-alt-icon ui-btn-right"></a>
        <!-- ✆ -->
    </div>

    <div class="wrap" style='margin-top:8px'>
        <div class="project flexbox">
            <div>
                <a href="javascript:document.location.href='${ctx}/mall/order/entrance'">
                    <div class="headico">
                        <img src="${ctx}/static/images/month.png" style='height:0.75em'>
                    </div>
                    <div class="proFont">
                        <p>包月保洁</p>

                        <p>4小时100元</p>
                    </div>
                </a>
            </div>

            <div>
                <a href="">
                    <div class="headico">
                        <img src="${ctx}/static/images/zsj.png">
                    </div>
                    <div class="proFont" style='margin-left:-6px'>
                        <p style="color:#52c4ff">室内大扫除</p>

                        <p>￥5元每平方米</p>
                    </div>
                </a>
            </div>


        </div>
        <nav class="svc">
            <ul>
                <li>
                    <ol class="flexbox">
                        <li><a href="#"> <i><img
                                src="${ctx}/static/images/newh.png"> </i><br> <span
                                class="px26">新居开荒</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href="#"> <i><img
                                src="${ctx}/static/images/glass.png"> </i><br> <span
                                class="px26">擦玻璃</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/sofa.png"> </i><br> <span
                                class="px26">沙发保养</span> <b class="quick"></b> </a></li>
                    </ol>
                </li>
                <li>
                    <ol class="flexbox">
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/kitchen.png"> </i><br> <span
                                class="px26">厨房高温保养</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/restroom.png"> </i><br> <span
                                class="px26">卫生间高温清洁</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href="${ctx}/device"> <i><img
                                src="${ctx}/static/images/floor.png"> </i><br> <span
                                class="px26">地板打蜡</span> <!--<b class="quick"></b>--> </a></li>
                    </ol>
                </li>
                <li>
                    <ol class="flexbox">
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/carpet.png"> </i><br> <span
                                class="px26">地毯清洗</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/curtain.png"> </i><br> <span
                                class="px26">窗帘清洗</span> <!--<b class="quick"></b>--> </a></li>
                        <li><a href=""> <i><img
                                src="${ctx}/static/images/laundry.png"> </i><br> <span
                                class="px26">洗衣服务</span> <!--<b class="quick"></b>--> </a></li>
                    </ol>
                </li>
            </ul>
        </nav>
        <button id="editAddr">editAddress</button>

        <footer>
            <!-- 	<ol class="flexbox px26">
    <li><a href="">首页</a></li>
    <li><a href="">会员中心</a></li>
    <li><a href="">常见问题</a></li>
    <li><a href="">服务说明</a></li>
</ol> -->
            <ol class="flexbox px26">
                <li style='color:#38c'>Copyright © 滨海迅洁
                </li>
                <li style='color:#38c'>津ICP备14007710号</li>

            </ol>
        </footer>
    </div>
</div>
<form name="form1">
  <input name="address1" type="text" />
  <input name="address2" type="text" />
  <input name="address3" type="text" />
  <input name="detail" type="text" />
  <input name="phone" type="text" />
</form>
</body>
<script src="${ctx}/static/jquery/jquery-1.9.1.min.js"></script>
<script src="${ctx}/static/jquery/jquery.mobile-1.4.5.min.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
    jQuery(function ($) {
        $("[data-role='navbar']").navbar();
        $("[data-role='header'], [data-role='footer']").toolbar();
    });
    $(document).on("pagecontainerchange", function () {
        var current = $(".ui-page-active").jqmData("title");
        $("[data-role='header'] h1").text(current);
        $("[data-role='navbar'] a.ui-btn-active").removeClass("ui-btn-active");
        $("[data-role='navbar'] a").each(function () {
            if ($(this).text() === current) {
                $(this).addClass("ui-btn-active");
            }
        });
    });

//    var sign;
//    function jsonpCallback(data) {
//        sign = data.sign;
//        wx.config({
//            debug: false,
//            appId: 'wx177697e0beee76d2',
//            timestamp: sign.timestamp,
//            nonceStr: sign.nonceStr,
//            signature: sign.signature,
//            jsApiList: [
//                // 所有要调用的 API 都要加到这个列表中
//                'onMenuShareTimeline',
//                'onMenuShareAppMessage',
//                'onMenuShareQQ'
//            ]
//        });
//
//
//    }
//    // 根据当前页面路径动态获取token。 类似 url?timeline=user
//    var str = "http://test.weixin.bigertech.com/api/sign?appId=wxd98888751036c960&callback=jsonpCallback&url=";
//    var href = encodeURIComponent(window.location.href);
//    var script_elem = document.createElement("script");
//    script_elem.src = str + href;
//    document.body.appendChild(script_elem);
//
//    $("#editAddr").on("click", function () {
//        WeixinJSBridge.invoke('editAddress', {
//                    "appId": "wxd98888751036c960",
//                    "scope": "jsapi_address",
//                    "signType": "sha1",
//                    "addrSign": "3b5d23bf52d0103d955518f63c41e86085aef3ea",
//                    "timeStamp": 1448292649,
//                    "nonceStr": "ad9e0146-934b-42d6-bc84-e55dd6b264c5"
//                },
//                function (res) {
//                    document.form1.address1.value = res.proviceFirstStageName;
//                    document.form1.address2.value = res.addressCitySecondStageName;
//                    document.form1.address3.value = res.addressCountiesThirdStageName;
//                    document.form1.detail.value = res.addressDetailInfo;
//                    document.form1.phone.value = res.telNumber;
//                })
//    });

</script>
</html>