<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=yes"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no"/>
    <title></title>
    <link rel="stylesheet"
          href="${ctx}/static/assets/css/ms.css">
    <link href="${ctx}/static/assets/js/mobiscroll-2.14.4/css/mobiscroll-2.14.4.min.css"
          rel="stylesheet" type="text/css"/>


    <script src="${ctx}/static/assets/js/jquery-1.10.2.min.js"></script>
    <script src="${ctx}/static/assets/js/mobiscroll-2.14.4/js/mobiscroll-2.14.4.min.js"
            type="text/javascript"></script>
</head>
<body>

<div class="demo-phone-scroll">

    <div class="contact-list demo-scroll">


        <div mbsc-enhance="" class="mbsc-comp mbsc-form mbsc-mobiscroll mbsc-ltr">
            <div class="mbsc-lv-cont mbsc-lv-mobiscroll mbsc-lv-ic-anim">
                <ul class="mbsc-lv mbsc-lv-dummy"></ul>
                <div class="mbsc-lv-sl-c" data-ref="1">
                    <ul class="contact-detail-edit mbsc-lv mbsc-lv-v mbsc-lv-sl-curr">
                        <li class="crm-form-cont mbsc-lv-item mbsc-lv-item-enhanced">
                            <form id="inputForm" class="form-horizontal" role="form"
                                    name="form" action="${ctx}/mall/order/common" method="post">
                                <div class="mbsc-divider">预留基本信息</div>
                                <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    您的姓名
                                                    </span><span class="mbsc-input-wrap"><input id="nickName"
                                                                                                name="nickName"
                                                                                                type="text"
                                                                                                value="${nickName}"
                                                                                                placeholder="如李小姐"
                                                                                                class="ng-pristine ng-valid mbsc-control mbsc-control-ev mbsc-active ng-touched"
                                                                                                style=""><span
                                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-user4"></span></span>
                                </label>

                                <%--<label class="mbsc-ic-left mbsc-input mbsc-textarea"><span class="mbsc-label">--%>
                                <%--Address--%>
                                <%--</span><span class="mbsc-input-wrap"><textarea placeholder="Address"--%>
                                <%--data-icon="location"--%>
                                <%--name="address"--%>
                                <%--ng-model="currentItems[$index].address"--%>
                                <%--class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"--%>
                                <%--style="height: 34px;"></textarea><span--%>
                                <%--class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-location"></span></span>--%>
                                <%--</label>--%>

                                <input type="hidden" name="addressId" id="addressId" value="${addressId}"/>
                                <label class="mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    请输入需要打扫的地址
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                placeholder="如贝肯山5-5-101"
                                                                                                data-icon="office"
                                                                                                name="addressDetail"
                                                                                                id="addressDetail"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-office"></span></span>
                                </label>

                                <%--<div class="mbsc-divider">Phone</div>--%>
                                <label
                                        class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">
                                                    请输入您联系电话
                                                    </span><span class="mbsc-input-wrap"><input type="tel"
                                                                                                placeholder="如15522227772"
                                                                                                name="telephone"
                                                                                                id="telephone"
                                                                                                data-icon="phone"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>
                                </label>
                                <%--<label--%>
                                <%--ng-repeat="phone in currentItems[$index].phones"--%>
                                <%--class="ng-binding ng-scope mbsc-ic-left mbsc-input"><span class="mbsc-label">--%>
                                <%--Work--%>
                                <%--</span><span class="mbsc-input-wrap"><input type="tel"--%>
                                <%--placeholder="Phone"--%>
                                <%--name="phone"--%>
                                <%--data-icon="phone"--%>
                                <%--ng-model="phone.number"--%>
                                <%--class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span--%>
                                <%--class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-phone"></span></span>--%>
                                <%--</label>--%>


                                <label
                                        class="ng-binding ng-scope mbsc-ic-left mbsc-input "><span class="mbsc-label">
                                                    请选择希望打扫的日期
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                id="startDate"
                                                                                                placeholder="点击选择即可"
                                                                                                name="startDate"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-fa-calendar"></span></span>
                                </label>

                                <label
                                        class="ng-binding ng-scope mbsc-ic-left mbsc-input clock2"><span
                                        class="mbsc-label">
                                                    请选择开始打扫的时间(早7点~晚18点)
                                                    </span><span class="mbsc-input-wrap"><input type="text"
                                                                                                id="startTime"
                                                                                                placeholder="点击选择即可"
                                                                                                name="startTime"
                                                                                                data-icon="clock"
                                                                                                class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                        class="mbsc-input-ic mbsc-left-ic mbsc-ic mbsc-ic-clock"></span></span>
                                </label>


                                <div class="mbsc-divider">选择收费方式</div>

                                <label
                                        class="ng-binding ng-scope mbsc-radio clock2"><input type="radio"
                                                                                             name="priceType"
                                                                                             value="COMMON_METER"
                                                                                             class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                        class="mbsc-radio-box"></span><span class="mbsc-label">1、按面积收费 ￥ 5 /㎡
                                </span>
                                </label><label
                                    class="ng-binding ng-scope mbsc-radio"><input type="radio" name="priceType"
                                                                                  value="COMMON_HOUR"
                                                                                  class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span
                                    class="mbsc-radio-box"></span><span class="mbsc-label">2、按工时收费 ￥ 40 /每人每小时
                                </span>
                            </label>
                                <%--<label--%>
                                <%--class="ng-binding ng-scope mbsc-checkbox"><input type="checkbox"--%>
                                <%--class="ng-pristine ng-untouched ng-valid mbsc-control mbsc-control-ev"><span--%>
                                <%--class="mbsc-checkbox-box"></span><span class="mbsc-label">好久没打扫了--%>
                                <%--</span>--%>
                                <%--</label>--%>

                                <div class="contact-detail-save mbsc-padding">
                                    <button type="submit" class="mbsc-btn-block mbsc-control mbsc-btn mbsc-control-ev"
                                            data-icon="checkmark"
                                            ><span
                                            class="mbsc-btn-ic mbsc-ic mbsc-ic-arrow-right5"></span>马上预约
                                    </button>
                                </div>


                            </form>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
<script>

    $(function () {
        $('#startDate').mobiscroll().calendar({
            buttons: [

                'set',
//                {
//                    text: 'Custom',
//                    icon: 'checkmark',
//                    cssClass: 'my-btn',
//                    handler: function (event, inst) {
//                        alert('Custom button clicked!');
//                    }
//                },

                'cancel',
                'now'
            ],
            mode: 'mixed', //日期选择模式
            layout: 'liquid',
//            liveSwipe:false,
            theme: 'mobiscroll',
            rows: 10,
            display: 'bottom',
            swipeDirection: 'vertical',
            weekCounter: 'year',
            lang: 'zh',
            markedText: true,
            closeOnSelect: true,
            focusOnClose: true,
//            counter:true,
//            closeOnOverlay:true,
//            calendarHeight:400,
//            height:20,
//            headerText:'打扫日期为:{value}',
//            nowText: "今天",

            marked: [{
                d: new Date(2016, 5, 25),
                text: '满',
                icon: 'arrow-down5',
                color: 'blue'
            }, {
                d: new Date(2016, 5, 26),
                text: '满',
                icon: 'meteo-rainy',
                color: 'blue'
            }, {
                d: new Date(2016, 5, 28),
                text: '满',
                icon: 'foundation-torsos-all',
                color: 'blue'
            }, {
                d: new Date(2016, 5, 29),
                text: '满',
                icon: 'meteo-rainy4',
                color: 'blue'
            }, {
                d: new Date(2016, 5, 30),
                text: '满',
                icon: 'meteo-rainy4',
                color: 'blue'
            }],
            invalid: [{
                d: new Date(2016, 5, 1)
            }, {
                d: new Date(2016, 5, 2)
            }, {
                d: new Date(2016, 5, 3)
            }]
        });


        var opt = {};
        opt.default = {
            theme: 'wp-light', //皮肤样式
            headerText: '开始时间:{value}',
            amText: 'am',
            display: 'bottom', //显示方式
            rows: 6,
//            mode: 'mixed', //日期选择模式
            lang: 'zh',
            stepMinute: 10,
            context: ".demo-phone-scroll",
            animate: 'pop',
            focusOnClose: true,
            tap: true,
            minDate: new Date(new Date().setHours(7, 0, 0, 0)),
            maxDate: new Date(new Date().setHours(18, 0, 0, 0)),
            defaultValue: new Date(new Date().setHours(8, 0, 0, 0)),
            invalid: [
//                '1/1', // 1st of January disabled
//                '12/24', // Christmas disabled
//                '12/25', // Christmas disabled
//                'w0', // Sundays disabled
                {start: '00:00', end: '6:59'}, // Every day
//                { start: '18:10', end: '23:59' }, // Every day
//                { start: '12:00', end: '12:59' },
//                { d: 'w6', start: '00:00', end: '08:59' }, // Saturday
//                { d: 'w6', start: '17:00', end: '23:59' }, // Saturday
//                new Date(2015,10,3), // exact date
//                new Date(2016,6,11) // exact date
            ]

//             ,wheels: [[
//                {
//                    keys: [3, 4, 5, 6],
//                    values: [3, 4, 5, 6]
//                },
//                {
//                    keys: [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 11],
//                    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
//                }
//            ]],
//            formatResult: function (data) {
//                // Sample result: "6 ft 8 in"
//                return data[0] + ' ft ' + data[1] + ' in';
//            }

        };

//        $("#appDateTime").val('').scroller('destroy').scroller($.extend(opt['date'], opt['default']));
//        var optDateTime = $.extend(opt['datetime'], opt['default']);
        var optTime = $.extend(opt['time'], opt['default']);
        $("#startTime").mobiscroll(optTime).time(optTime);


//        var currYear = (new Date()).getFullYear();
//        var opt={};
//        opt.date = {preset : 'date'};
//        //opt.datetime = { preset : 'datetime', minDate: new Date(2012,3,10,9,22), maxDate: new Date(2014,7,30,15,44), stepMinute: 5  };
//        opt.datetime = {preset : 'datetime'};
//        opt.time = {preset : 'time'};
//        opt.default = {
//            theme: 'android-ics light', //皮肤样式
//            display: 'modal', //显示方式
//            mode: 'scroller', //日期选择模式
//            lang:'zh',
//            showNow: true,
//            invalid: { h: [0, 6]} ,
//
//            startYear:currYear - 10, //开始年份
//            endYear:currYear + 10 //结束年份
//        };
//
//        var optDateTime = $.extend(opt['datetime'], opt['default']);
//
//        $("#appDateTime").mobiscroll(optDateTime).datetime(optDateTime);


    })
</script>
<style>
    .mbsc-android-holo .dwv {
        text-align: left;
        text-indent: .8em;
    }
</style>
</html>