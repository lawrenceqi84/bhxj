CREATE TABLE `operation_t`
(
        `id` VARCHAR(64) ,
            `user_id` INT(64) ,
            `goods_id` INT(64) ,
            `order_id` VARCHAR(64) ,
            `order_detail_id` VARCHAR(64) ,
            `operation_date` VARCHAR(64) ,
            `plan_start_time` VARCHAR(64) ,
            `plan_end_time` VARCHAR(64) ,
            `plan_hour` ${fieldInfo.dbType}(64) ,
            `plan_price` ${fieldInfo.dbType}(64) ,
            `real_start_time` VARCHAR(64) ,
            `real_end_time` VARCHAR(64) ,
            `real_hour` ${fieldInfo.dbType}(64) ,
            `real_price` ${fieldInfo.dbType}(64) ,
            `pay_status` INT(64) ,
            `job_status` INT(64) ,
            `evaluate` ${fieldInfo.dbType}(0) ,
            `evaluate_content` VARCHAR(64) ,
            `own_employee_id` INT(64) ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;