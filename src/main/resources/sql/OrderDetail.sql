CREATE TABLE `order_detail_t`
(
        `id` INT(11)NOT NULL AUTO_INCREMENT,
            `order_id` INT(64) ,
            `goods_id` INT(64) ,
            `goods_price` ${fieldInfo.dbType}(64) ,
            `goods_title` VARCHAR(64) ,
            `goods_image` VARCHAR(64) ,
            `count` INT(64) ,
            `address_detail` VARCHAR(64) ,
            `supplier_id` INT(64) ,
            `supplier_name` VARCHAR(64) ,
            `express_id` VARCHAR(64) ,
            `express_name` VARCHAR(64) ,
            `express_no` VARCHAR(64) ,
            `send_status` VARCHAR(64) ,
            `send_time` VARCHAR(64) ,
            `send_detail` VARCHAR(64) ,
            `tprice` ${fieldInfo.dbType}(0) ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;