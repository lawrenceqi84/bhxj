CREATE TABLE `supplier_t`
(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
            `username` VARCHAR(50)  ,
            `pwd` VARCHAR(50)  ,
            `nickname` VARCHAR(100)  ,
            `company` VARCHAR(10)  ,
            `linkman` VARCHAR(20)  ,
            `telephone` VARCHAR(11)  ,
            `p` VARCHAR(10)  ,
            `c` VARCHAR(10)  ,
            `d` VARCHAR(10)  ,
            `address` VARCHAR(100)  ,
            `start_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
            `end_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;