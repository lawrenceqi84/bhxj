CREATE TABLE `user_address_t`
(
        `id` VARCHAR(64) ,
            `user_id` INT(64) ,
            `telephone` VARCHAR(64) ,
            `receiver` VARCHAR(64) ,
            `p` VARCHAR(64) ,
            `c` VARCHAR(64) ,
            `d` VARCHAR(64) ,
            `properties` VARCHAR(64) ,
            `detail` VARCHAR(64) ,
            `size` ${fieldInfo.dbType}(64) ,
            `real_size` ${fieldInfo.dbType}(64) ,
            `people` INT(64) ,
            `postcode` VARCHAR(64) ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;