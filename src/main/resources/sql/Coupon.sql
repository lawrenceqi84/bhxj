CREATE TABLE `coupon_t`
(
        `id` VARCHAR(64) ,
            `user_id` VARCHAR(40) ,
            `name` VARCHAR(100) ,
            `code` VARCHAR(8) ,
            `price` ${fieldInfo.dbType}(64) ,
            `type` VARCHAR(20)NOT NULL ,
            `start_time` VARCHAR(10)NOT NULL ,
            `end_time` VARCHAR(10)NOT NULL ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;