CREATE TABLE `order_t`
(
        `id` INT(11)NOT NULL AUTO_INCREMENT,
            `user_id` INT(64) ,
            `price` VARCHAR(32) ,
            `tprice` VARCHAR(32) ,
            `real_price` VARCHAR(64) ,
            `coupon_id` INT(64) ,
            `coupon_price` VARCHAR(64) ,
            `address_id` INT(64) ,
            `address_detail` VARCHAR(64) ,
            `buy_time` VARCHAR(64) ,
            `send_time` VARCHAR(64) ,
            `send_status` VARCHAR(64) ,
            `order_status` VARCHAR(64) ,
            `pay_way` VARCHAR(64) ,
            `pay_way_detail` VARCHAR(64) ,
            `shopping_count` INT(64) ,
            `delivery_by_self_yn` VARCHAR(64) ,
            `best_employee_id` INT(64) ,
            `own_employee_id` INT(64) ,
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;