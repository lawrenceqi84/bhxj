package com.bhxj.utils.wechat.entity;

/**
 * Created by LawrenceQi on 15/11/23.
 */
public class MsgType {
    public static String Image = "Image";
    public static String Text = "Text";
    public static String Event = "Event";
}
