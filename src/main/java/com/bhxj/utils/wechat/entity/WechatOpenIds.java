package com.bhxj.utils.wechat.entity;

import java.util.List;

/**
 * Created by LawrenceQi on 15/11/17.
 */
public class WechatOpenIds {
    private int total;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    private int count;
    private Data data;

    public class Data {
        public List<String> getOpenid() {
            return openid;
        }

        public void setOpenid(List<String> openid) {
            this.openid = openid;
        }

        private List<String> openid;
    }
}
