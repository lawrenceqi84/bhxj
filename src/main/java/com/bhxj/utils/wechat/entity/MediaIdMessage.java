package com.bhxj.utils.wechat.entity;

/**
 * Created by LawrenceQi on 15/11/23.
 */

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class MediaIdMessage {
    @XStreamAlias("MediaId")
    @XStreamCDATA
    private String MediaId;

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }

}