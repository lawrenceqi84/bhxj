package com.bhxj.utils.wechat;

import com.bhxj.entity.User;
import com.bhxj.service.UserService;
import com.bhxj.utils.EmojiFilter;
import com.google.gson.Gson;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.*;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Created by LawrenceQi on 15/11/15.
 */
public class WechatUtils {
    private static Logger logger = LoggerFactory.getLogger(WechatUtils.class);
    public static Gson gson = new Gson();
    @Autowired
    private UserService userService;
    @Autowired
    WxMpInMemoryConfigStorage wxMpStorage;
    @Autowired
    WxMpServiceImpl wxMpService;
    private static final String OAUTH2_GET_USER_OPEN_ID = "snsapi_base";
    private static final String OAUTH2_GET_USER_INFO = "snsapi_userinfo";


    public int access() {

        WxMpMessageRouter router = new WxMpMessageRouter(wxMpService);
        WxMpMessageInterceptor interceptor = null;
        WxMpMessageHandler handler = null;
        router.rule().event("EVENT").interceptor(interceptor).handler(handler).end();
        return 0;
    }


    public void operateEvent(String eventType, String openId) throws WxErrorException {
        if (eventType.equals(WxConsts.EVT_SUBSCRIBE)) {
            long count = userService.countUserByOpenId(openId);
            WxMpUser userInfo = wxMpService.userInfo(openId, null);
            User user = setUser(userInfo);
            if (count > 0) {
                userService.updateUserForWx(user);
                logger.info("[用户取消后又关注]NICKNAME={},时间={}", user.getNickname(), new Date().toString());
            } else {
                logger.info("[新用户关注]NICKNAME={},时间={}", user.getNickname(), new Date().toString());
                userService.saveUser(user);
            }
        } else if (eventType.equals(WxConsts.EVT_UNSUBSCRIBE)) {
            logger.info("[用户取消关注]OPENID={},时间={}", openId, new Date().toString());
            User user = new User();
            user.setOpenid(openId);
            user.setSubscribe(false);
            user.setSubscribeTime(new Date());
            userService.updateUserForWxUnSub(user);
        }
    }

    /**
     * oauth2.0第一步获取拼接url 通过url回掉得到code
     */
    public String getReqUrl(String url)
    {
        return wxMpService.oauth2buildAuthorizationUrl(url, OAUTH2_GET_USER_INFO, null);
    }
    /**
     * oauth2.0第二步获取token
     * oauth2.0第三步为刷新token
     * oauth2.0第四步抓去用户信息
     */
    public WxMpUser getUserInfoByAccess(String code) throws Exception {
        WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
        WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken,null);
        //WxMpUser wxMpUser = wxMpService.userInfo(openId,null);//普通方式，尚未测试 TODO
        return wxMpUser;
    }

    public int syncUserToDB()  throws WxErrorException{
        int count = 0;
            wxMpStorage.setAccessToken(wxMpService.getAccessToken());
            WxMpUserList wxMpUserList = wxMpService.userList("");
            if (wxMpUserList.getCount() != -1) {
                for (String openId : wxMpUserList.getOpenIds()) {
                    WxMpUser userInfo = wxMpService.userInfo(openId, null);
                    User user = setUser(userInfo);
                    if (userService.countUserByOpenId(user.getOpenid()) == 0) {
                        userService.saveUser(user);
                    }
                }
            }
        return count;
    }


    public User setUser(WxMpUser userInfo) {
        User user = new User();
        user.setTelephone("");
        user.setPwd("");
        user.setVcode(784533);
        user.setUsername(EmojiFilter.filterEmoji("未填"));
        user.setNickname(EmojiFilter.filterEmoji(userInfo.getNickname()));
        user.setSex(userInfo.getSex());
        user.setAge(1);
        user.setBirthday("未填");
        user.setJob("未填");
        user.setCompany("未填");
        //user.setLoginTime(new Date());
        user.setSubscribe(userInfo.getSubscribe());
        user.setEmail("未填");
        user.setCurrentMoney(0.0);
        user.setLevel(9);
        user.setOpenid(userInfo.getOpenId());
        user.setUnionid(userInfo.getUnionId());
        user.setGroupid(userInfo.getGroupId());
        user.setHeadimgurl(userInfo.getHeadImgUrl());
        user.setSubscribeTime(new Date(userInfo.getSubscribeTime() * 1000));
        user.setRemark(EmojiFilter.filterEmoji(userInfo.getRemark()));
        user.setLanguage(userInfo.getLanguage());
        user.setCountry(userInfo.getCountry());
        user.setProvince(userInfo.getProvince());
        user.setCity(userInfo.getCity());
        return user;
    }


    public static void main(String[] args){
        String aa = ("20170101123");
        try {
            System.out.println(aa.substring(8));
        } catch (Exception e) {
            System.out.println("12312312312312");
            //// FIXME: 07/06/2017
        }
    }


}
