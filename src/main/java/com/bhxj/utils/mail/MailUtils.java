package com.bhxj.utils.mail;

import com.bhxj.entity.ConfigEmail;
import com.bhxj.service.sys.ConfigEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class MailUtils {
    private static Logger logger = LoggerFactory.getLogger(MailUtils.class);

    @Autowired
    private ConfigEmailService configEmailService;
    ConfigEmail configEmail;

    public void init() {
        //configEmail = configEmailService.getConfigEmail();
//        logger.info(">> Nofitication Mail SMTP:{} , Account:{} , Password:{}, Subject:{}", configEmail.getSmtp(), configEmail.getAccount(), configEmail.getPassword(), configEmail.getSubject());
    }

    private String account;

    private String password;

    private String subject;


    private String smtp;


    public boolean sendMail(String to, String content) {
        try {
            configEmail = configEmailService.getConfigEmail();
            logger.info("sendMail to:{}  content:{} ", to, content);

            Properties props = new Properties();
            props.put("mail.smtp.host", getSmtp());
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(getAccount(),
                                    getPassword());
                        }
                    });
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(getAccount()));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(to));
                message.setSubject(getSubject());
                MimeMultipart mainPart = new MimeMultipart("alternative");
                BodyPart html = new MimeBodyPart();
                html.setContent(content, "text/html;charset=utf-8");
                mainPart.addBodyPart(html);
                message.setContent(mainPart);
                Transport.send(message);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return false;
            }
            return true;
        } catch (Exception ee) {
            logger.error(ee.getMessage(), ee);
            return false;
        }
    }

    public String getAccount() {
        return configEmail.getAccount();
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return configEmail.getPassword();
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubject() {
        return configEmail.getSubject();
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSmtp() {
        return configEmail.getSmtp();
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }


//	public static void main(String[] args){
//		sendMail("lqi@comcat.cn", "123");
//	}


}

