package com.bhxj.utils;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;


public class HttpUtils {
    private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static String sendGet(String url) {
        String result = "";
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // 建立实际的连接
            conn.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = conn.getHeaderFields();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += "\n" + line;
            }
        } catch (Exception e) {
            logger.error("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static String sendPost(String url, String params) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(params);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            result = IOUtils.toString(conn.getInputStream());
        } catch (Exception e) {
            logger.error("发送POST请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void sendHttpRequest(String urlStr, String sendContent) {
        //发送http请求
        OutputStream outputStrm = null;
        HttpURLConnection urlConnection = null;
        try {
            logger.info("给云管理发送的内容：[" + sendContent + "]");
            URL url = new URL(urlStr);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Connection", "close");
            urlConnection.setRequestProperty("Content-Length", String.valueOf(sendContent.getBytes().length));
            urlConnection.setRequestProperty("Content-type", "application/octet-stream");
            outputStrm = urlConnection.getOutputStream();
            outputStrm.write(sendContent.getBytes("utf-8"));
            outputStrm.flush();
            int responseCode = urlConnection.getResponseCode();
            if (responseCode == 200) {
                logger.info("发送http请求成功");
            }
        } catch (Exception e) {
            logger.error("发送HTTP请求出错 [" + e.getMessage() + "]");
        } finally {
            if (outputStrm != null) {
                try {
                    outputStrm.close();
                } catch (Exception e) {
                    logger.error("关闭 ObjectOutputStream出错[" + e.getMessage() + "]");
                }
            }
        }
    }


    public static void postTracing(String restUrl, Object obj) {
        try {
            URL url = new URL(restUrl);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            DataOutputStream out = new DataOutputStream(
                    connection.getOutputStream());


            ObjectMapper objectMapper = new ObjectMapper();
            JsonGenerator jg = objectMapper.getJsonFactory().createJsonGenerator(System.out,
                    JsonEncoding.UTF8);
            objectMapper.writeValue(System.out, obj);
            objectMapper.writeValue(out, obj);
            out.flush();
            out.close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String lines;
            StringBuffer sb = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                sb.append(lines);
            }
            System.out.println(sb.toString());
            logger.info(sb.toString());
            reader.close();
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public static void main(String[] args){
//        String ACCESS_TOKEN = WechatUtils.getAccessToken();
//        System.out.println(ACCESS_TOKEN);;
//        String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token="+ACCESS_TOKEN+"&next_openid=";
//        System.out.println(HttpUtils.sendGet(url));
        //
    }


}
