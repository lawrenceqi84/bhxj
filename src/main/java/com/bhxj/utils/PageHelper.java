package com.bhxj.utils;

import com.bhxj.repository.PageParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

/**
 * Created by Jean on 7/8/15.
 */
public class PageHelper {
    public static PageParams buildPageParams(int pageNumber, int pageSize) {
        return new PageParams(pageNumber, pageSize);
    }

    public static PageParams buildPageParams(int pageNumber, int pageSize, Map<String, Object> searchParams) {
        PageParams pageParams = buildPageParams(pageNumber, pageSize);
        if(searchParams != null && !searchParams.isEmpty()) {
            String[] keyInfo;
            for(String key:searchParams.keySet()) {
                keyInfo = key.split("_");
                if(keyInfo.length > 1) {
                    pageParams.put(key.substring(key.indexOf("_") + 1), searchParams.get(key));
                }
            }
        }

        return pageParams;
    }

    public static PageParams buildPageParams(int pageNumber, int pageSize, Map<String, Object> searchParams, String sortType) {
        PageParams pageParams = buildPageParams(pageNumber, pageSize, searchParams);
        if (sortType != null && !"".equals("sortType")) {
            if("auto".equals(sortType)){
                pageParams.put("sortType","id");
            } else {
                pageParams.put("sortType",sortType);
            }
        }

        return pageParams;
    }

    public static <T> Page<T> buildPagedResult(List<T> entities, PageParams pageParams, long total) {
        return new PageImpl<T>(entities, new PageRequest( pageParams.getPageNumber(), pageParams.getPageSize()), total);
    }
}
