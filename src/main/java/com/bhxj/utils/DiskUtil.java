package com.bhxj.utils;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;

/**
 * Created by LawrenceQi on 15/8/14.
 */
public class DiskUtil {

    public String getRootDir() {
        return rootDir;
    }

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }
    @Value("${resource.root.dir}")
    public  String rootDir;


    public  final int BYTE_TO_GB = 1024 * 1024 * 1024;

    public  long getUsedSpace() throws Exception {
        String os = System.getProperty("os.name");
        if (os.startsWith("Windows")) {
            return getUsedSpaceOnWindows();
        } else if (os.startsWith("Linux") || os.startsWith("Mac")) {
            return getUsedSpaceOnLinux();
        }
        throw new UnsupportedOperationException(
                "The method getFreeSpace(String path) has not been implemented for this operating system.");
    }

    public  long getFreeSpace() throws Exception {
        String os = System.getProperty("os.name");
        if (os.startsWith("Windows")) {
            return getFreeSpaceOnWindows();
        } else if (os.startsWith("Linux") || os.startsWith("Mac")) {
            return getFreeSpaceOnLinux();
        }
        throw new UnsupportedOperationException(
                "The method getFreeSpace(String path) has not been implemented for this operating system.");
    }

    private  long getUsedSpaceOnWindows() throws Exception {
        File root = new File(rootDir.substring(0,rootDir.indexOf(":"))+"://");
        // File rootE = new File("e://");
        long used = (root.getTotalSpace() - root.getFreeSpace()) / BYTE_TO_GB;
        long total = root.getTotalSpace() / BYTE_TO_GB;
        return used * 100 / total;
    }

    private  long getUsedSpaceOnLinux() throws Exception {
        File disk = new File("/");
        long receive = disk.getFreeSpace() - disk.getUsableSpace();
        double usagePercent = 100.0 * (disk.getTotalSpace() - disk.getFreeSpace()) / (disk.getTotalSpace() - receive);
        return (int) Math.ceil(usagePercent);
    }

    private  long getFreeSpaceOnWindows() throws Exception {
        File root = new File(rootDir.substring(0,rootDir.indexOf(":"))+"://");
        return root.getFreeSpace() / BYTE_TO_GB;
    }

    private  long getFreeSpaceOnLinux() throws Exception {
        File disk = new File("/");
        return disk.getFreeSpace() / BYTE_TO_GB;
    }

    public static void main(String[] args) throws Exception {
//        System.out.println(DiskUtil.getFreeSpace());
        String test = "F:/newTest";
        System.out.println(test.substring(0,test.indexOf(":"))+"~~~");
    }
}
