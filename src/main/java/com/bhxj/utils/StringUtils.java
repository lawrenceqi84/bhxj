package com.bhxj.utils;

import java.util.UUID;

/**
 * Created by Jean on 7/10/15.
 */
public class StringUtils {
    public static String genUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
