package com.bhxj.utils.tree;

/**
 * Created by LawrenceQi on 15/7/17.
 */
public class Item {
    /**
     * 节点的名字
     */
    private String name ;

    /**
     * 节点的类型："item":文件  "folder":目录
     */
    private String type ;

    /**
     * 子节点的信息
     */
    private AdditionalParameters additionalParameters ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType()
    {
        return type ;
    }

    public void setType(String type )
    {
        this .type = type;
    }

    public AdditionalParameters getAdditionalParameters()
    {
        return additionalParameters ;
    }

    public void setAdditionalParameters(AdditionalParameters additionalParameters )
    {
        this .additionalParameters = additionalParameters ;
    }

}
