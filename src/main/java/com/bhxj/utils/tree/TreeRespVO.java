package com.bhxj.utils.tree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by LawrenceQi on 15/7/17.
 */
public class TreeRespVO {
    private String key;

    private Map<String,Item> data = new HashMap<String,Item>();

    public Map<String, Item> getData() {
        return data;
    }

    public void setData(Map<String, Item> data) {
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
