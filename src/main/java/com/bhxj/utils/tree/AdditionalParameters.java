package com.bhxj.utils.tree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by LawrenceQi on 15/7/17.
 */
public class AdditionalParameters {
    /**
     * 子节点列表
     */
    private Map<String,Item> children = new HashMap<String,Item>();

    /**
     * 节点的Id
     */
    private String id;

    /**
     * 是否有选中属性
     */
//    @JsonProperty("item-selected")
//    private boolean itemSelected;

    public Map<String, Item> getChildren() {
        return children;
    }

    public void setChildren(Map<String, Item> children) {
        this.children = children;
    }

//    public void setItemSelected(boolean itemSelected) {
//        this.itemSelected = itemSelected;
//    }
//
//
//    public boolean isItemSelected() {
//        return itemSelected;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
