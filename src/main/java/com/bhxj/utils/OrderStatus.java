package com.bhxj.utils;

/**
 * Created by LawrenceQi on 16/6/24.
 */
public class OrderStatus {
    public class PayStatus{
        public static final String NO_PAY = "0";
        public static final String PAY_SUCCESS = "1";
        public static final String PAY_FAIL = "2";
    }

    public class Status{
        public static final String NO_SEND = "0";
        public static final String SENT = "1";
        public static final String RECIEVED = "2";
        public static final String REJECTED = "3";
        public static final String BACK_M_ING = "4";
        public static final String BACK_M_SUCCESS = "5";
        public static final String BACK_M_FAIL= "6";

    }
}
