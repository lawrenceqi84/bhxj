package com.bhxj.utils;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;

/**
 * Created by Jean on 8/31/15.
 */
public class ConfigUtils {


    @Value("${resource.root.dir}")
    private String CONFIG_RESOURCE_ROOT;

    @Value("${image.dir}")
    private String CONFIG_IMAGE_DIR;

    private String TOMCAT_MAPPING_PATH = "bhxj";

    public StringBuilder getTemplateImageDir(String titleId) {
        StringBuilder imageDir = new StringBuilder(50)
                .append(CONFIG_RESOURCE_ROOT).append(File.separator).append(CONFIG_IMAGE_DIR).append(File.separator)
                .append(titleId).append(File.separator);
        return imageDir;
    }

    public String getConfigResourceRoot() {
        return CONFIG_RESOURCE_ROOT;
    }

    public String getConfigUploadDir() {
        return CONFIG_IMAGE_DIR;
    }


}
