/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.web.systems;

import com.bhxj.entity.Supplier;
import com.bhxj.service.SupplierService;
import com.bhxj.service.account.ShiroDbRealm;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * LoginController负责打开登录页面(GET请求)和登录出错页面(POST请求)，
 * <p/>
 * 真正登录的POST请求由Filter完成,
 *
 * @author LawrenceQi
 */
@Controller
@RequestMapping(value = "/systems/")
public class LoginController {
    @Autowired
    private SupplierService supplierService;

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET})
    public String systems() {
        return "systems/login";
    }

    @RequestMapping(value = "login", method = {RequestMethod.POST, RequestMethod.GET})
    public String login(Model model, HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (StringUtils.isBlank(username)) {
            return "systems/login";
        }
        Supplier supplier = supplierService.findSupplierByUsernameAndPwd(username, password);
        if (supplier == null) {
            model.addAttribute("error", "用户名或密码错误！");
            return "systems/login";
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("supplier", supplier);
            return "systems/home";
        }
    }

//    @RequestMapping(method = RequestMethod.POST)
//    public String fail(@RequestParam(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String userName, Model model) {
//        model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, userName);
//        return "account/login";
//    }

    @RequestMapping(value = "logout", method = {RequestMethod.POST, RequestMethod.GET})
    public String logout(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("error", "成功退出!");
        HttpSession session = request.getSession();
        session.removeAttribute("supplier");
        session.removeAttribute("cart");
        return "systems/login";
    }


    private String getCurrentUserName() {
        ShiroDbRealm.ShiroUser user = (ShiroDbRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return user.loginName;
    }

}
