/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.web.systems.goods;

import com.bhxj.entity.Goods;
import com.bhxj.entity.JsonResponse;
import com.bhxj.entity.Supplier;
import com.bhxj.service.GoodsService;
import com.bhxj.utils.ConfigUtils;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springside.modules.web.Servlets;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Goods管理的Controller, 使用Restful风格的Urls:
 * <p/>
 * List page : GET /systems/goods/
 * Create page : GET /systems/goods/create
 * Create action : POST /systems/goods/create
 * Update page : GET /systems/goods/update/{uuid}
 * Update action : POST /systems/goods/update
 * Delete action : GET /systems/goods/delete/{uuid}
 *
 * @author calvin
 */
@Controller
@RequestMapping(value = "/systems/goods")
public class GoodsController {
    private Logger logger = LoggerFactory.getLogger(GoodsController.class);

    private static final String PAGE_SIZE = "10";

    private static Map<String, String> sortTypes = Maps.newLinkedHashMap();

    static {
        sortTypes.put("create_Time", "开始时间");
        sortTypes.put("Goods_Name", "商品名");
    }

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ConfigUtils configUtils;

    @RequestMapping(method = RequestMethod.GET)
    public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
                       @RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
                       @RequestParam(value = "sortType", defaultValue = "auto") String sortType, Model model,
                       HttpServletRequest request) {
        if (checkAdmin(request)) return "error/404";
        Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
        Page<Goods> goods = goodsService.findAllGoods(searchParams, pageNumber, pageSize, sortType);
        model.addAttribute("goodsList", goods);
        model.addAttribute("sortType", sortType);
        model.addAttribute("sortTypes", sortTypes);
        model.addAttribute("searchParams", Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
        return "systems/goods/list";


    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model, HttpServletRequest request) {
        if (checkAdmin(request)) return "error/404";
        model.addAttribute("goods", new Goods());
        model.addAttribute("action", "create");
        return "systems/goods/form";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("goods") Goods goods
            , RedirectAttributes redirectAttributes
            , HttpServletRequest request, Model model
//            , @RequestParam("imgUpload") CommonsMultipartFile file
// , HttpServletRequest request
    ) {
        try {
            if (checkAdmin(request)) return "error/404";
            if (goodsService.findOneByTitleId(goods.getTitleId()) > 0) {
                redirectAttributes.addFlashAttribute("succFlag", false);
                model.addAttribute("succFlag", false);
                model.addAttribute("message", "该商品已经存在!!!!");
                model.addAttribute("goods", goods);
                model.addAttribute("action", "create");
                //return "redirect:/systems/goods/create";
                return "systems/goods/form";
            }
            //if (!file.isEmpty() && createPic(file, goods))
            else {
                redirectAttributes.addFlashAttribute("message", "创建商品成功");
                redirectAttributes.addFlashAttribute("succFlag", true);
                goodsService.saveGoods(goods);
            }

        } catch (Exception e) {
            logger.error("创建商品有误:{}", e.getMessage());
            model.addAttribute("succFlag", false);
            model.addAttribute("message", "该商品已经存在!!!!");
            model.addAttribute("goods", goods);
            model.addAttribute("action", "create");
            return "systems/goods/form";
        }
        return "redirect:/systems/goods/";
    }
                            //http://shang.qq.com/email/stop/email_stop.html?qq=2798453818&sig=310ca973e45ae78690ea37d6d5a38cf3cf50b9431afdca1c&tttt=1

    @RequestMapping(value = "update/{id:.+}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") long id, Model model, HttpServletRequest request) {
        if (checkAdmin(request)) return "error/404";
        Goods goods = goodsService.findOneGoods(id);
        model.addAttribute("goods", goods);
        model.addAttribute("action", "update");
        return "systems/goods/form";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("goods") Goods goods, @Valid String[] tags, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (checkAdmin(request)) return "error/404";
        try {
            goodsService.updateGoods(goods);
            //saveGoodsTag(goods.getNxPlanName(), tagList, parentList); todo 保存商品图片 先删再插
        } catch (Exception e) {
            logger.error("删除商品有误:{}", e.getMessage());
        }
        redirectAttributes.addFlashAttribute("message", "更新商品成功");
        return "redirect:/systems/goods/";
    }

    @RequestMapping(value = "delete/{id:.+}")
    public String delete(@PathVariable("id") long id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (checkAdmin(request)) return "error/404";
        goodsService.delete(id);
        redirectAttributes.addFlashAttribute("message", "删除商品成功");
        //return "redirect:/goods/";
        return "redirect:/systems/goods/";
    }


    /**
     * 取出session中的供货商.
     */

    private Supplier getCurrentSupplier(HttpServletRequest request) {
        Object supplier = request.getSession().getAttribute("supplier");
        if (supplier != null) {
            return (Supplier) supplier;
        }
        return null;
    }

    /**
     * 检查访问者是否是超级管理员
     */
    private boolean checkAdmin(HttpServletRequest request) {
        Supplier supplier = getCurrentSupplier(request);
        if (supplier == null || !"admin".equals(supplier.getUsername())) {
            return true;
        }
        return false;
    }

    private boolean createPic(CommonsMultipartFile file, Goods goods) throws IOException {
        boolean createImageFlag = false;
        StringBuilder imageDirPathBuilder = configUtils.getTemplateImageDir(goods.getTitleId());

        //String fileDirPath = IMAGEMATCH_DIR + dir;
        //判断根文件目录是否存在
        File fileDir = new File(imageDirPathBuilder.toString());
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        /** 判断是否有图片文件,进行删除 **/
        /** 更新需要将原先图进行删除 **/
        String comFileName = file.getOriginalFilename();
        String fileFullPath = "";
        File localFile = null;

        fileFullPath = new StringBuilder()
                .append(imageDirPathBuilder)
                .append(goods.getTitleId())
                .append(comFileName.substring(comFileName.lastIndexOf(".")))
                .toString();

        logger.info(fileFullPath);
        localFile = new File(fileFullPath);

        if (localFile != null && !localFile.exists()) {
            FileOutputStream fos = null;
            try {
                localFile.createNewFile();
                byte[] bytes = file.getBytes();
                fos = new FileOutputStream(localFile.getAbsoluteFile());
                fos.write(bytes);
                goods.setImage(fileFullPath);
                createImageFlag = true;
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            } finally {
                if (fos != null) {
                    fos.close();
                }
            }
            logger.info("The image file does not exist, add a new image file!");
        } else {
            logger.info("The image file exists!");
        }
        return createImageFlag;
    }

    @RequestMapping(value = "mateName/{imageMatchName}", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse checkSameName(@PathVariable("imageMatchName") long imageMatchName) {
        String jsonMessage = "该图像模板名称不存在,可以使用!";
        boolean ok = false;
        if (goodsService.findOneGoods(imageMatchName) != null) {
            jsonMessage = "该问题名称已经存在!";
            ok = true;
        }
        return new JsonResponse(ok, jsonMessage);
    }

}
