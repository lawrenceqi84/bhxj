/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.web.mall.order;

import com.bhxj.entity.*;
import com.bhxj.service.*;
import com.bhxj.service.account.ShiroDbRealm;
import com.bhxj.utils.StringUtils;
import com.bhxj.utils.wechat.WechatUtils;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Home的Controller, 使用Restful风格的Urls:
 *
 * @author LawrenceQi
 */
@Controller
@RequestMapping(value = "/mall/order")
public class OrderController {
    private Logger logger = LoggerFactory.getLogger(OrderController.class);

    private static String PRICE_TYPE_COMMON_METER = "COMMON_METER";
    private static String PRICE_TYPE_COMMON_HOUR = "COMMON_HOUR";
    private static final String OAUTH2_BACK_URL = "http://www.bhxj.com.cn/mall/order/c";
    private static final String OAUTH2_DENY_CODE = "authdeny";
    //    private String addressId;
//    private String nickname;                                    /mall/order/c
//    private String addressDetail;
//    private String telephone;
//    private String priceType;
//    private String startDate;
//    private String startTime;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private CouponService couponService;
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private WechatUtils wechatUtils;
    @Autowired
    private UserService userService;


//    getUserInfoByAccess

    @RequestMapping(value = "entrance", method = RequestMethod.GET)
    public void entrance(Model model, HttpServletRequest request, HttpServletResponse response) {
        try {
            String url = wechatUtils.getReqUrl(OAUTH2_BACK_URL);
            response.sendRedirect(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequestMapping(value = "c", method = RequestMethod.GET)
    public String c(Model model, HttpServletRequest request) {
        User bhxjUser = null;
        try {
            String code = request.getParameter("code");
            if (OAUTH2_DENY_CODE.equals(code)) {
                return "error/404";//跳转到未授权页面
            } else {
                WxMpUser user = wechatUtils.getUserInfoByAccess(code);
                String openId = user.getOpenId();
                if(userService.countUserByOpenId(openId)==0){
                    return "error/404"; //跳转到未关注 请关注的页面
                }
                bhxjUser = wechatUtils.setUser(user);
                request.getSession().setAttribute("user", bhxjUser);
            }

            //User user = getCurrentUser(request);
            //if (user == null) return "error/404";
            model.addAttribute("nickName",bhxjUser.getNickname());
            model.addAttribute("bhxjUser",bhxjUser);
//            model.addAttribute("testingDevice", deviceService.countTestingDevice());
//            model.addAttribute("used", diskUtil.getUsedSpace());
//            model.addAttribute("free", diskUtil.getFreeSpace());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "mall/order/orderC";
    }

    @RequestMapping(value = "common", method = RequestMethod.POST)
    public String common(HttpServletRequest request, Model model) {//@Valid @ModelAttribute("order") Order order,
        try {
            if (!checkLogin(request)) return "error/404";


            String nickName = request.getParameter("nickName");
            String telephone = request.getParameter("telephone");
            String addressId = request.getParameter("addressId");
            String addressDetail = request.getParameter("addressDetail");
            String startDate = request.getParameter("startDate");
            String startTime = request.getParameter("startTime");
            String priceType = request.getParameter("priceType");
            int hour = Integer.valueOf(request.getParameter("count"));
            int employeeNum = Integer.valueOf(request.getParameter("employee_num"));
            String couponId = request.getParameter("couponId");

            Order cart = getCurrentCart(request);
            if (cart == null) {      //购物车内无商品直接添加
                Goods goods = goodsService.findGoodsByTitleId(priceType);
                cart = addToCart(cart, goods, employeeNum, hour);
                //此逻辑可用到单纯添加购物车上
            }


            if (org.apache.commons.lang.StringUtils.isBlank(addressId)) {
                addressId = StringUtils.genUUID();
                UserAddress address = new UserAddress();
                address.setTelephone(telephone);
                address.setReceiver(nickName);
                address.setDetail(addressDetail);
                userAddressService.saveUserAddress(address);  //新建服务地址
                cart.setAddressId(addressId);
            }
            HttpSession session = request.getSession();
            session.setAttribute("cart", cart);      //设置session中的购物车
            User user = getCurrentUser(request);     //获取用户信息
            String orderId = StringUtils.genUUID();
            cart.setId(orderId);                        //订单主键设置
            cart.setUserId(user.getId());               //订单用户赋值
            cart.setAddressDetail(addressDetail);       //订单地址明细赋值
            cart.setOrderStatus(OrderStatus.PayStatus.NO_PAY);   //订单付款状态设置
            cart.setPayWay(OrderStatus.PayWay.WEIXIN);  //订单付款方式设置
            if (org.apache.commons.lang.StringUtils.isNotBlank(couponId)) {   //如果使用优惠券则为订单设置优惠券ID和金额
                Coupon coupon = couponService.findOneCoupon(couponId);
                if (coupon != null) {
                    cart.setCouponId(couponId);
                    cart.setCouponPrice(coupon.getPrice());
                }
            }

            BigDecimal totalPrice = saveOrderDetail(cart);  //保存订单明细 并返回总费用
            cart.setPrice(totalPrice.doubleValue());        //订单费用赋值
            orderService.saveOrder(cart);   //保存订单


//            if (priceType.equals(PRICE_TYPE_COMMON_HOUR)) {
//                Goods goods = goodsService.findGoodsByTitleId(priceType);
//                addToCart(cart,goods,1);
//            } else if (priceType.equals(PRICE_TYPE_COMMON_METER)) {
//                Goods goods = goodsService.findGoodsByTitleId(priceType);
//                addToCart(cart,goods,1);
//            }
//            if (goodsService.findGoodsByTitleId(goods.getTitleId()) > 0) {
//                redirectAttributes.addFlashAttribute("succFlag", false);
//                model.addAttribute("succFlag", false);
//                model.addAttribute("message", "该商品已经存在!!!!");
//                model.addAttribute("goods", goods);
//                model.addAttribute("action", "create");
//                return "systems/goods/form";
//            }
//            else {
//                redirectAttributes.addFlashAttribute("message", "创建商品成功");
//                redirectAttributes.addFlashAttribute("succFlag", true);
//                goodsService.saveGoods(goods);
//            }

        } catch (Exception e) {
            logger.error("下单失败:{}", e.getMessage());
            model.addAttribute("succFlag", false);
            model.addAttribute("message", "下单失败，请致电客服 400-617-0503");
//            model.addAttribute("order", cart);
            return "systems/goods/form";
        }
        return "mall/order/orderC";
    }

    /***
     * @param order
     * @return 保存订单明细，返回订单总费用
     */
    private BigDecimal saveOrderDetail(Order order) {

        Map<Long, OrderDetail> orderMap = order.getOdMap();
        BigDecimal price = null;
        BigDecimal count = null;
        BigDecimal employeeNum = null;
        BigDecimal goodsTotalPrice = null;
        BigDecimal totalPrice = new BigDecimal(0);
//        BigDecimal tPrice = new BigDecimal(0);
//        StringBuilder orderBody = new StringBuilder();
        for (Map.Entry<Long, OrderDetail> entry : orderMap.entrySet()) {
            OrderDetail od = entry.getValue();
            orderDetailService.saveOrderDetail(od);
            price = new BigDecimal(od.getGoodsPrice());
            count = new BigDecimal(od.getCount());
            employeeNum = new BigDecimal(od.getEmployeeNum());
            goodsTotalPrice = price.multiply(count).multiply(employeeNum);
            totalPrice = totalPrice.add(goodsTotalPrice);
        }

        if (org.apache.commons.lang.StringUtils.isNotBlank(order.getCouponId())) {
            BigDecimal couponPrice = new BigDecimal(order.getCouponPrice());
            totalPrice = totalPrice.subtract(couponPrice);
        }
        return totalPrice;

    }

    /**
     * 检查访问者是否是超级管理员
     */
    private boolean checkLogin(HttpServletRequest request) {
        User user = getCurrentUser(request);
        if (user == null) {
            return false;
        }
        return true;
    }

    /**
     * 取出session中的供货商.
     */

    private User getCurrentUser(HttpServletRequest request) {
        Object user = request.getSession().getAttribute("user");
        if (user != null) {
            return (User) user;
        }
        return null;
    }

    private Order getCurrentCart(HttpServletRequest request) {
        Object order = request.getSession().getAttribute("cart");
        if (order != null) {
            return (Order) order;
        }
        return null;
    }


    private String getCurrentUserName() {
        ShiroDbRealm.ShiroUser user = (ShiroDbRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return user.loginName;
    }


    public Order addToCart(Order order, Goods goods, int employeeNum, int hour) throws Exception {
        Map<Long, OrderDetail> odMap = null;
        if (order.getOdMap() == null) {// 完全无购物记录
            odMap = new HashMap<Long, OrderDetail>();
            OrderDetail od = new OrderDetail();
            od.setOrderId(order.getId());
            od.setCount(hour);
            od.setEmployeeNum(employeeNum);
            od.setGoodsId(goods.getId());
            od.setGoodsPrice(goods.getCurrentPrice());
            od.setGoodsTitle(goods.getTitle());
            od.setGoodsImage(goods.getImage());
            od.setTprice(goods.getTprice());
            od.setSupplierId(goods.getSupplierId());
            odMap.put(goods.getId(), od);
            order.setOdMap(odMap);
        } else {
            odMap = order.getOdMap();
            OrderDetail od = odMap.get(goods.getId());
            if (od != null) {// 有购物记录且又购买了同样商品故数量累加
                int tempcount = od.getCount();
                od.setCount(tempcount + hour);
                // odMap.put(g.getId(), od);
            } else {// 有购物记录，又追加了新的商品
                od = new OrderDetail();
                od.setOrderId(order.getId());
                od.setCount(hour);
                od.setEmployeeNum(employeeNum);
                od.setGoodsId(goods.getId());
                od.setGoodsPrice(goods.getCurrentPrice());
                od.setGoodsTitle(goods.getTitle());
                od.setGoodsImage(goods.getImage());
                od.setSupplierId(goods.getSupplierId());
                for (Map.Entry<Long, OrderDetail> entry : odMap.entrySet()) {
                    OrderDetail eachOd = entry.getValue();
                    if (eachOd.getSupplierId().equals(goods.getSupplierId())) {
                        if (eachOd.getTprice() <= goods.getTprice()) {
                            eachOd.setTprice(0.0);
                            od.setTprice(goods.getTprice());
                        } else {
                            od.setTprice(0.0);
                        }
                    }
                }
                if (od.getTprice() == null) {
                    od.setTprice(goods.getTprice());
                }
                odMap.put(goods.getId(), od);
            }
        }
        return order;
    }

    /***
     * 转移通供货商的邮费逻辑写的有问题 todo
     */
    public Order removeFromCart(Order order, Goods goods) throws Exception {
        Map<Long, OrderDetail> odMap = order.getOdMap();
        odMap.remove(goods.getId());
        for (Map.Entry<Long, OrderDetail> entry : odMap.entrySet()) {
            OrderDetail eachOd = entry.getValue();
            if (eachOd.getSupplierId().equals(goods.getSupplierId())) {
                eachOd.setTprice(goodsService.findOneGoods(eachOd.getGoodsId()).getTprice());
                break;           //转移供货商的邮费逻辑写的有问题 todo
            }
        }
        return order;
    }

    public static Order deliverySelfCart(Order order, String delivery) throws Exception {
        order.setDeliveryBySelfYn(delivery);
        return order;
    }

    /*
     * 因前台逻辑复杂 暂时不用
     */
    public static Order changeNumFromCart(Order order, Goods goods, int count) throws Exception {
        Map<Long, OrderDetail> odMap = order.getOdMap();
        OrderDetail od = odMap.get(goods.getId());
        od.setCount(count);
        return order;
    }

    public CouponService getCouponService() {
        return couponService;
    }

    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }


    public OrderDetailService getOrderDetailService() {
        return orderDetailService;
    }

    public void setOrderDetailService(OrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public GoodsService getGoodsService() {
        return goodsService;
    }

    public void setGoodsService(GoodsService goodsService) {
        this.goodsService = goodsService;
    }


    public UserAddressService getUserAddressService() {
        return userAddressService;
    }

    public void setUserAddressService(UserAddressService userAddressService) {
        this.userAddressService = userAddressService;
    }

    public WechatUtils getWechatUtils() {
        return wechatUtils;
    }

    public void setWechatUtils(WechatUtils wechatUtils) {
        this.wechatUtils = wechatUtils;
    }


    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    public static void main(String[] args) {
//        File[] roots = File.listRoots();
//        for (File _file : roots) {
//            System.out.println(_file.getPath());
//            System.out.println(_file.getName());
//            System.out.println("Free space = " + _file.getFreeSpace() / 1024 / 1024 / 1024);
//            System.out.println("Usable space = " + _file.getUsableSpace() / 1024 / 1024 / 1024);
//            System.out.println("Total space = " + _file.getTotalSpace() / 1024 / 1024 / 1024);
//            System.out.println();
//        }


//        File win = new File("C:\\WINDOWS");
//        System.out.println(win.getPath());
//        System.out.println(win.getName());
//        System.out.println("Free space = " + win.getFreeSpace());
//        System.out.println("Usable space = " + win.getUsableSpace());
//        System.out.println("Total space = " + win.getTotalSpace());
//        System.out.println();

        long a = 12;
        int v = 1234;
        System.out.println(String.format("%.2f", Math.floor(a * 100) / v));
        Map<String,String> testMap = new HashMap<String,String>();
        testMap.put("1","111");
        testMap.put("2","222");
        testMap.put("3", "333");
        testMap.put("4", "444");
        testMap.put("5", "555");
        for (String key:testMap.keySet()){
            System.out.println(testMap.get(key));
            String aaa = testMap.get("1");
            aaa = "1111";
            testMap.put("1",aaa)       ;

        }
        for (String key:testMap.keySet()){
            System.out.println(testMap.get(key));

        }
    }

}
