/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.web.mall;

import com.bhxj.service.account.ShiroDbRealm;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Home的Controller, 使用Restful风格的Urls:
 *
 * @author LawrenceQi
 */
@Controller
@RequestMapping(value = "/index")
public class IndexController {
//    @Autowired
//    DeviceService deviceService;
//    @Autowired
//    DiskUtil diskUtil;

    @RequestMapping(method = RequestMethod.GET)
    public String turnToIndex(Model model) {
        try {
//            model.addAttribute("userName", getCurrentUserName());
//            model.addAttribute("testingDevice", deviceService.countTestingDevice());
//            model.addAttribute("used", diskUtil.getUsedSpace());
//            model.addAttribute("free", diskUtil.getFreeSpace());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "mall/index";
    }


    private String getCurrentUserName() {
        ShiroDbRealm.ShiroUser user = (ShiroDbRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return user.loginName;
    }

    public static void main(String[] args) {
//        File[] roots = File.listRoots();
//        for (File _file : roots) {
//            System.out.println(_file.getPath());
//            System.out.println(_file.getName());
//            System.out.println("Free space = " + _file.getFreeSpace() / 1024 / 1024 / 1024);
//            System.out.println("Usable space = " + _file.getUsableSpace() / 1024 / 1024 / 1024);
//            System.out.println("Total space = " + _file.getTotalSpace() / 1024 / 1024 / 1024);
//            System.out.println();
//        }


//        File win = new File("C:\\WINDOWS");
//        System.out.println(win.getPath());
//        System.out.println(win.getName());
//        System.out.println("Free space = " + win.getFreeSpace());
//        System.out.println("Usable space = " + win.getUsableSpace());
//        System.out.println("Total space = " + win.getTotalSpace());
//        System.out.println();

        long a = 12;
        int v = 1234;
        System.out.println(String.format("%.2f", Math.floor(a * 100) / v));


    }

}
