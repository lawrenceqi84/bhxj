

package com.bhxj.service;

import com.bhxj.entity.User;
import com.bhxj.repository.mybatis.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class UserService {
    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;

    public void saveUser(User user) {
        userDao.save(user);
    }

    public User findOneUser(long id) {
        return userDao.findOne(id);
    }

    public void updateUser(User entity) {
        userDao.update(entity);
    }

    public List<User> findAllUser() {
        return userDao.findAll();
    }

    public long count() {
        return userDao.count();
    }

    public void updateUserForWx(User entity) {
        userDao.updateForWx(entity);
    }

    public void updateUserForWxUnSub(User entity) {
        userDao.updateForWxUnSub(entity);
    }

    public long countUserByOpenId(String opendId) {
        return userDao.countByOpenId(opendId);
    }

}