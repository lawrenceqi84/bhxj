

package com.bhxj.service;

import com.bhxj.entity.UserAddress;
import com.bhxj.repository.mybatis.UserAddressDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class UserAddressService {
    private static Logger logger = LoggerFactory.getLogger(UserAddressService.class);

    @Autowired
    private UserAddressDao userAddressDao;

    public void saveUserAddress(UserAddress userAddress) {
        userAddressDao.save(userAddress);
    }

    public UserAddress findOneUserAddress(String id) {
        return userAddressDao.findOne(id);
    }

    public void updateUserAddress(UserAddress entity) {
        userAddressDao.update(entity);
    }

    public List<UserAddress> findAllUserAddress() {
        return userAddressDao.findAll();
    }

    public long count() {
        return userAddressDao.count();
    }
}