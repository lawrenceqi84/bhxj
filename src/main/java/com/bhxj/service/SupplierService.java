

package com.bhxj.service;

import com.bhxj.entity.Supplier;
import com.bhxj.repository.mybatis.SupplierDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class SupplierService {
    private static Logger logger = LoggerFactory.getLogger(SupplierService.class);

    @Autowired
    private SupplierDao supplierDao;

    public void saveSupplier(Supplier supplier) {
        supplierDao.save(supplier);
    }

    public Supplier findOneSupplier(long id) {
        return supplierDao.findOne(id);
    }

    public void updateSupplier(Supplier entity) {
        supplierDao.update(entity);
    }

    public List<Supplier> findAllSupplier() {
        return supplierDao.findAll();
    }

    public long count() {
        return supplierDao.count();
    }
    public Supplier findSupplierByUsernameAndPwd(String username,String pwd) {
        return supplierDao.findByUsernameAndPwd(username,pwd);
    }
}