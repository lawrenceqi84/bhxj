package com.bhxj.service.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

/**
 * Created by Jean on 8/14/15.
 */
@Component
public class CacheService {

    @CacheEvict(value = "taskStatistics")
    public void evictTaskStatistic(String nxTaskUuid) {
    }
}
