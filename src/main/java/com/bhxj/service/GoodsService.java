

package com.bhxj.service;

import com.bhxj.entity.Goods;
import com.bhxj.repository.PageParams;
import com.bhxj.repository.mybatis.GoodsDao;
import com.bhxj.utils.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Component
@Transactional
public class GoodsService {
    private static Logger logger = LoggerFactory.getLogger(GoodsService.class);

    @Autowired
    private GoodsDao goodsDao;

    public void saveGoods(Goods goods) {
        goodsDao.save(goods);
    }

    public Goods findOneGoods(long id) {
        return goodsDao.findOne(id);
    }

    public Goods findGoodsByTitleId(String titleId) {
        return goodsDao.findGoodsByTitleId(titleId);
    }

    public long findOneByTitleId(String titleId) {
        return goodsDao.findOneByTitleId(titleId);
    }


    public void updateGoods(Goods entity) {
        goodsDao.update(entity);
    }

    public List<Goods> findAllGoods() {
        return goodsDao.findAll();
    }

    public long count() {
        return goodsDao.count();
    }

    public void delete(long id) {
        goodsDao.delete(id);
    }

    public Page<Goods> findAllGoods(Map<String, Object> searchParams, int pageNumber, int pageSize,
                                    String sortType) {
        PageParams pageParams = PageHelper.buildPageParams(pageNumber, pageSize);
        if (searchParams != null && !searchParams.isEmpty()) {
            String[] keyInfo;
            for (String key : searchParams.keySet()) {
                keyInfo = key.split("_");
                if (keyInfo.length == 2) {
                    pageParams.put(keyInfo[1], searchParams.get(key));
                }
            }
        }
        long total = goodsDao.count(pageParams);
        if ("auto".equals(sortType)) {
            pageParams.put("sortType", "uuid");
        } else {
            pageParams.put("sortType", sortType);
        }
        List<Goods> tasks = goodsDao.findAll(pageParams);
        return PageHelper.buildPagedResult(tasks, pageParams, total);
    }
}