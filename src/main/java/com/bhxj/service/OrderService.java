

package com.bhxj.service;

import com.bhxj.entity.Order;
import com.bhxj.repository.mybatis.OrderDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class OrderService {
    private static Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderDao orderDao;

    public void saveOrder(Order order) {
        orderDao.save(order);
    }

    public Order findOneOrder(long id) {
        return orderDao.findOne(id);
    }

    public void updateOrder(Order entity) {
        orderDao.update(entity);
    }

    public List<Order> findAllOrder() {
        return orderDao.findAll();
    }

    public long count() {
        return orderDao.count();
    }
}