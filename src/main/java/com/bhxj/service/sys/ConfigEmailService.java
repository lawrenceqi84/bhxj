/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.service.sys;

import com.bhxj.entity.ConfigEmail;
import com.bhxj.repository.mybatis.ConfigEmailDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component        // Spring Bean的标识.
@Transactional    // 类中所有public函数都纳入事务管理的标识.
public class ConfigEmailService {

    private ConfigEmailDao configEmailDao;
    private static Logger logger = LoggerFactory.getLogger(ConfigEmailService.class);

    @CacheEvict(value = "configEmail", allEntries = true)
    public void updateConfigEmail(ConfigEmail entity) {
        configEmailDao.update(entity);
    }
    @Cacheable(value = "configEmail", key = "#root.methodName")
    public ConfigEmail getConfigEmail() {
        return configEmailDao.findConfigEmail();
    }
    @Autowired
    public void setConfigEmailDao(ConfigEmailDao configEmailDao) {
        this.configEmailDao = configEmailDao;
    }
}
