

package com.bhxj.service;

import com.bhxj.entity.Coupon;
import com.bhxj.repository.mybatis.CouponDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class CouponService {
    private static Logger logger = LoggerFactory.getLogger(CouponService.class);

    @Autowired
    private CouponDao couponDao;

    public void saveCoupon(Coupon coupon) {
        couponDao.save(coupon);
    }

    public Coupon findOneCoupon(String id) {
        return couponDao.findOne(id);
    }

    public void updateCoupon(Coupon entity) {
        couponDao.update(entity);
    }

    public List<Coupon> findAllCoupon() {
        return couponDao.findAll();
    }

    public long count() {
        return couponDao.count();
    }
}