

package com.bhxj.service;

import com.bhxj.entity.OrderDetail;
import com.bhxj.repository.PageParams;
import com.bhxj.repository.mybatis.OrderDetailDao;
import com.bhxj.utils.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Transactional
public class OrderDetailService {
    private static Logger logger = LoggerFactory.getLogger(OrderDetailService.class);

    @Autowired
    private OrderDetailDao orderDetailDao;

    public void saveOrderDetail(OrderDetail orderDetail) {
        orderDetailDao.save(orderDetail);
    }

    public OrderDetail findOneOrderDetail(long id) {
        return orderDetailDao.findOne(id);
    }

    public void updateOrderDetail(OrderDetail entity) {
        orderDetailDao.update(entity);
    }

    public List<OrderDetail> findAllOrderDetail() {
        return orderDetailDao.findAll();
    }

    public long count() {
        return orderDetailDao.count();
    }
}