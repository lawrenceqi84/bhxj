

package com.bhxj.service;

import com.bhxj.entity.Operation;
import com.bhxj.repository.mybatis.OperationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class OperationService {
    private static Logger logger = LoggerFactory.getLogger(OperationService.class);

    @Autowired
    private OperationDao operationDao;

    public void saveOperation(Operation operation) {
        operationDao.save(operation);
    }

    public Operation findOneOperation(String id) {
        return operationDao.findOne(id);
    }

    public void updateOperation(Operation entity) {
        operationDao.update(entity);
    }

    public List<Operation> findAllOperation() {
        return operationDao.findAll();
    }

    public long count() {
        return operationDao.count();
    }
}