/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.bhxj.rest;

import com.bhxj.utils.wechat.SHA1;
import com.bhxj.utils.wechat.SerializeXmlUtil;
import com.bhxj.utils.wechat.WechatUtils;
import com.thoughtworks.xstream.XStream;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;
import java.io.IOException;
import java.util.*;

/**
 * Device的Restful API的Controller.
 *
 * @author LawrenceQi
 */
@RestController
@RequestMapping(value = "/api/v1/wechat")
public class WechatRestController {
    private String Token = "bhxjasdf123";
    private static Logger logger = LoggerFactory.getLogger(WechatRestController.class);


    @Autowired
    private Validator validator;
    @Autowired
    private WechatUtils wechatUtils;

//	@RequestMapping(method = RequestMethod.GET, produces = MediaTypes.JSON_UTF_8)
//	public List<Device> list() {
//		return deviceService.getAllDevice();
//	}                            api/v1/wechat/

    @RequestMapping(value = "/receive", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void get(Model model, HttpServletRequest request, HttpServletResponse response) {
        boolean isGet = request.getMethod().toLowerCase().equals("get");
        if (isGet) {
            String signature = request.getParameter("signature");
            String timestamp = request.getParameter("timestamp");
            String nonce = request.getParameter("nonce");
            String echostr = request.getParameter("echostr");
            logger.info("signature:" + signature);
            logger.info("timestamp:" + timestamp);
            logger.info("nonce:" + nonce);
            logger.info("echostr:" + echostr);
            access(request, response);
        } else {
            // 进入POST聊天处理
            try {
                // 接收消息并返回消息
                acceptMessage(request, response);
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }


    @RequestMapping(value = "/syncUser", method = RequestMethod.GET)
    public void syncUser() {
        try {
            wechatUtils.syncUserToDB();
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/subscribeOrNot", method = {RequestMethod.GET, RequestMethod.POST})
    public void subscribeOrNot(HttpServletRequest request, HttpServletResponse response) {
        try {
            acceptMessage(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 验证URL真实性
     *
     * @param request
     * @param response
     * @return String
     * @author lawrence
     * @date 2015年11月15日 上午1:53:07
     */
    private String access(HttpServletRequest request, HttpServletResponse response) {
        // 验证URL真实性
        String signature = request.getParameter("signature");// 微信加密签名
        String timestamp = request.getParameter("timestamp");// 时间戳
        String nonce = request.getParameter("nonce");// 随机数
        String echostr = request.getParameter("echostr");// 随机字符串
        List<String> params = new ArrayList<String>();
        params.add(Token);
        params.add(timestamp);
        params.add(nonce);
        // 1. 将token、timestamp、nonce三个参数进行字典序排序
        Collections.sort(params, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        // 2. 将三个参数字符串拼接成一个字符串进行sha1加密
        String temp = SHA1.encode(params.get(0) + params.get(1) + params.get(2));
        if (temp.equals(signature)) {
            try {
                response.getWriter().write(echostr);
                logger.info("成功返回 echostr：" + echostr);
                return echostr;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.error("失败 认证");
        return null;
    }


    private void acceptMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 处理接收消息
        ServletInputStream in = request.getInputStream();
        // 将POST流转换为XStream对象
        XStream xs = SerializeXmlUtil.createXstream();
        xs.processAnnotations(WxMpXmlMessage.class);
//        xs.processAnnotations(WxMpXmlMessage.class);
        // 将指定节点下的xml节点数据映射为对象
        xs.alias("xml", WxMpXmlMessage.class);
        // 将流转换为字符串
        StringBuilder xmlMsg = new StringBuilder();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1; ) {
            xmlMsg.append(new String(b, 0, n, "UTF-8"));
        }
        // 将xml内容转换为InputMessage对象
        WxMpXmlMessage wxMessage = (WxMpXmlMessage) xs.fromXML(xmlMsg.toString());
        //inputMsg = (InputMessage) xs.fromXML(xmlMsg.toString());

        String servername = wxMessage.getToUserName();// 服务端
        String openId = wxMessage.getFromUserName();// 客户端
//        long createTime = wxMessage.getCreateTime();// 接收时间
//        Long returnTime = Calendar.getInstance().getTimeInMillis() / 1000;// 返回时间
        String msgType = wxMessage.getMsgType();
        String event = wxMessage.getEvent();
        logger.info("[接收用户消息]openId={},createTime={},msgType={}", openId, new Date().toString(), msgType);

        if (WxConsts.XML_MSG_EVENT.equals(msgType)) {
            wechatUtils.operateEvent(event,openId);
            response.getWriter().write((""));
        }


//        response.getWriter().write((""));
//
//        if (msgType.equals(MsgType.Event)) {
//            String event = wxMessage.getEvent();
//            if (event.equals("")) {
//
//            }
//        }
//
//        // 根据消息类型获取对应的消息内容
//        else if (msgType.equals(MsgType.Text.toString())) {
//            // 文本消息
//            System.out.println("开发者微信号：" + wxMessage.getToUserName());
//            System.out.println("发送方帐号：" + wxMessage.getFromUserName());
//            System.out.println("消息创建时间：" + wxMessage.getCreateTime() + new Date(createTime * 1000l));
//            System.out.println("消息内容：" + wxMessage.getContent());
//            System.out.println("消息Id：" + wxMessage.getMsgId());
//
//            StringBuffer str = new StringBuffer();
//            str.append("<xml>");
//            str.append("<ToUserName><![CDATA[" + openId + "]]></ToUserName>");
//            str.append("<FromUserName><![CDATA[" + servername + "]]></FromUserName>");
//            str.append("<CreateTime>" + returnTime + "</CreateTime>");
//            str.append("<MsgType><![CDATA[" + msgType + "]]></MsgType>");
//            str.append("<Content><![CDATA[你说的是：" + wxMessage.getContent() + "，吗？]]></Content>");
//            str.append("</xml>");
//            System.out.println(str.toString());
//            response.getWriter().write(str.toString());
//        }
//        // 获取并返回多图片消息
//        else if (msgType.equals(MsgType.Image.toString())) {
//            System.out.println("获取多媒体信息");
//            System.out.println("多媒体文件id：" + wxMessage.getMediaId());
//            System.out.println("图片链接：" + wxMessage.getPicUrl());
//            System.out.println("消息id，64位整型：" + wxMessage.getMsgId());
//
//            OutputMessage outputMsg = new OutputMessage();
//            outputMsg.setFromUserName(servername);
//            outputMsg.setToUserName(openId);
//            outputMsg.setCreateTime(returnTime);
//            outputMsg.setMsgType(msgType);
//            ImageMessage images = new ImageMessage();
//            images.setMediaId(wxMessage.getMediaId());
//            outputMsg.setImage(images);
//            System.out.println("xml转换：/n" + xs.toXML(outputMsg));
//            response.getWriter().write(xs.toXML(outputMsg));
//
//        }
    }


//    @RequestMapping(method = RequestMethod.POST, consumes = MediaTypes.JSON)
//    public ResponseEntity<?> create(@RequestBody Device device, UriComponentsBuilder uriBuilder) {
//        // 调用JSR303 Bean Validator进行校验, 异常将由RestExceptionHandler统一处理.
//        BeanValidators.validateWithException(validator, device);
//
//        Device deviceByUUID = null;//deviceService.getDevice(device.getUuid());
//        logger.info("device sync" + device.getName() + " " + device.getPort() + " " + device.getUuid());
//        if (deviceByUUID == null) {
//
//            logger.info("device sync 没有该终端" + device.getName() + " " + device.getPort() + " " + device.getUuid());
//        } else {
//            logger.info("device sync 已存在该终端" + device.getName() + " " + device.getPort() + " " + device.getUuid());
//            if (deviceByUUID.getPort() != device.getPort()) {
//                logger.info("device sync 更改port" + device.getName() + " " + deviceByUUID.getPort() + " -> " + device.getPort() + " " + device.getUuid());
//                deviceByUUID.setPort(device.getPort());
//              //  deviceService.updateDevice(deviceByUUID);
//            }
//        }
//        // 按照Restful风格约定，创建指向新任务的url, 也可以直接返回id或对象.
//        String id = device.getUuid();
//        URI uri = uriBuilder.path("/api/v1/device/" + id).build().toUri();
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(uri);
//        return new ResponseEntity(headers, HttpStatus.CREATED);
//    }

//    @RequestMapping(value = "/{uuid}", method = RequestMethod.PUT, consumes = MediaTypes.JSON)
//    // 按Restful风格约定，返回204状态码, 无内容. 也可以返回200状态码.
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void update(@RequestBody User device) {
//        System.out.println("--------------------" + 2);
//        // 调用JSR303 Bean Validator进行校验, 异常将由RestExceptionHandler统一处理.
////		BeanValidators.validateWithException(validator, device);
////
////		// 保存任务
////		deviceService.saveDevice(device);
//    }
//
//    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void delete(@PathVariable("uuid") String uuid) {
//
//        System.out.println("--------------------" + 3);
//       // deviceService.deleteDevice(uuid);
//    }

    public static void main(String[] args) {
//        String aa = "<xml>\n" +
//                "<ToUserName>123</ToUserName>\n" +
//                "<FromUserName>456</FromUserName>\n" +
//                "<CreateTime>123456789</CreateTime>\n" +
//                "<MsgType>event</MsgType>\n" +
//                "<Event>subscribe</Event>\n" +
//                "</xml>\n";
//        XStream xstream = new XStream();
//        xstream.autodetectAnnotations(true);
//        xstream.alias("xml", SubscribeXml.class);
//        SubscribeXml obj = (SubscribeXml) xstream.fromXML(aa);
//

//        1449324948421
        Date date = new Date();
        String time = "1449321947" + "000";
        date.setTime(Long.valueOf(time));
        //date.setTime(System.currentTimeMillis());
//        System.out.println(date.toString());
//        Long test  = Long.valueOf((1449321947*1000));
//        System.out.println(test);
        System.out.println(date.toString());


    }

}
