package com.bhxj.repository;

import java.util.HashMap;

/**
 * Created by Jean on 7/8/15.
 */
public class PageParams extends HashMap<String, Object> {
    private int pageNumber;
    private int pageSize;
    private int offset;

    private long unionPageSize;     //add by 092 @ 2015-8-21

    public PageParams(int pageNumber, int pageSize) {
        // mysql index 0
        this.pageNumber = pageNumber - 1;
        this.pageSize = pageSize;

        this.offset = (pageNumber - 1) * pageSize;

        this.unionPageSize = pageNumber * pageSize;
        this.setUnionPageSize(unionPageSize);
        this.setPageSize(pageSize);
        this.setOffset(this.offset);
    }

    public PageParams() {

    }

    public static final String OFFSET = "offset";
    public static final String PAGE_SIZE = "pageSize";
    public static final String UNION_PAGE_SIZE = "unionPageSize";       //add by 092 @ 2015-8-21

    public void setOffset(int offset) {
        this.put(OFFSET, offset);
    }
    public void setPageSize(int pageSize) {
        this.put(PAGE_SIZE, pageSize);
    }

    public void setUnionPageSize(long unionPageSize) {
        this.put(UNION_PAGE_SIZE,unionPageSize);
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getOffset() {
        return offset;
    }

    public long getUnionPageSize() {
        return this.unionPageSize;
    }
}
