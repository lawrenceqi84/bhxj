package com.bhxj.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jean on 7/8/15.
 */
/*
  mybatis 不支持方法重载, mapper中禁止配置同id的tag
 */
public interface CommonDao<T, ID extends Serializable> {
    List<T> findAll();

    List<T> findAll(PageParams pageParams);

    T findOne(ID id);

    <S extends T> void save(S entity);

    void delete(ID id);


    void update(T entity);

    long count();

    long count(PageParams pageParams);
}
