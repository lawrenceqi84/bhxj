
package com.bhxj.repository.mybatis;

import com.bhxj.entity.Goods;
import com.bhxj.repository.CommonDao;

@MyBatisRepository
public interface GoodsDao extends CommonDao<Goods, Long> {
    long findOneByTitleId(String titleId);
    Goods findGoodsByTitleId(String titleId);
}
