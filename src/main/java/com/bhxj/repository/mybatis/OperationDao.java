
package com.bhxj.repository.mybatis;

import com.bhxj.entity.Operation;
import com.bhxj.repository.CommonDao;

import java.util.List;

@MyBatisRepository
public interface OperationDao extends CommonDao<Operation, String> {
}
