
package com.bhxj.repository.mybatis;

import com.bhxj.entity.UserAddress;
import com.bhxj.repository.CommonDao;

import java.util.List;

@MyBatisRepository
public interface UserAddressDao extends CommonDao<UserAddress, String> {
}
