package com.bhxj.repository.mybatis;

import com.bhxj.entity.ConfigEmail;
import com.bhxj.repository.CommonDao;
import com.bhxj.repository.PageParams;

/**
 * Created by  on 6/8/15.
 */

@MyBatisRepository
public interface ConfigEmailDao extends CommonDao<ConfigEmail, Long> {

    long count(PageParams pageParams);


    ConfigEmail findConfigEmail();

}
