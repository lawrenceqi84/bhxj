
package com.bhxj.repository.mybatis;

import com.bhxj.entity.Supplier;
import com.bhxj.repository.CommonDao;

@MyBatisRepository
public interface SupplierDao extends CommonDao<Supplier, Long> {
    Supplier findByUsernameAndPwd(String username,String pwd);
}
