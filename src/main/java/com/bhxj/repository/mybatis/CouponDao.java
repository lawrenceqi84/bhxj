
package com.bhxj.repository.mybatis;

import com.bhxj.entity.Coupon;
import com.bhxj.repository.CommonDao;

import java.util.List;

@MyBatisRepository
public interface CouponDao extends CommonDao<Coupon, String> {
}
