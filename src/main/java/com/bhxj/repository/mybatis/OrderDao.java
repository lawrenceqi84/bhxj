
package com.bhxj.repository.mybatis;

import com.bhxj.entity.Order;
import com.bhxj.repository.CommonDao;

@MyBatisRepository
public interface OrderDao extends CommonDao<Order, Long> {
}
