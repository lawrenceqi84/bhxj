
package com.bhxj.repository.mybatis;

import com.bhxj.entity.User;
import com.bhxj.repository.CommonDao;

@MyBatisRepository
public interface UserDao extends CommonDao<User, Long> {
    User findByLoginName(String loginName);

    long countByOpenId(String openId);

    void updateForWx(User user);

    void updateForWxUnSub(User user);
}
