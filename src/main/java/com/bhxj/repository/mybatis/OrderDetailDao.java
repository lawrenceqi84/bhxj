
package com.bhxj.repository.mybatis;

import com.bhxj.entity.OrderDetail;
import com.bhxj.repository.CommonDao;

import java.util.List;

@MyBatisRepository
public interface OrderDetailDao extends CommonDao<OrderDetail, Long> {
}
