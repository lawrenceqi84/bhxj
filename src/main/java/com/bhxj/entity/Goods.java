package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Id;
import code.gen.annotations.Table;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by LawrenceQi on 15/11/18.
 */
@Entity
@Table(name = "goods_t")
public class Goods {
    @Id
    private Long id;
    @Column(name = "title", nullable = true, length = 500)
    private String title;
    @Column(name = "title_id", nullable = true, length = 50)
    private String titleId;
    @Column(name = "description", nullable = true, length = 6)
    private String description;
    @Column(name = "goods_style", nullable = true)
    private int goodsStyle;
    @Column(name = "market_price", nullable = false, length = 10)
    private Double marketPrice;
    @Column(name = "current_price", nullable = false, length = 10)
    private Double currentPrice;
    @Column(name = "save_price", nullable = false, length = 10)
    private Double savePrice;
    @Column(name = "status", nullable = false, length = 1)
    private int status;
    @Column(name = "discount", nullable = false, length = 10)
    private Double discount;
    @Column(name = "sale_time", nullable = false)
    private Date saleTime;
    @Column(name = "end_time", nullable = false)
    private Date endTime;
    @Column(name = "amount", nullable = true)
    private int amount;
    @Column(name = "image", nullable = true, length = 255)
    private String image;
    @Column(name = "sale_num", nullable = true)
    private int saleNum;



    @Column(name = "stock_num", nullable = true)
    private int stockNum;
    @Column(name = "remain_num", nullable = true)
    private int remainNum;
    @Column(name = "collect_num", nullable = true, length = 11)
    private int collectNum;
    @Column(name = "supplier_id", nullable = true, length = 20)
    private Long supplierId;
    @Column(name = "tprice", nullable = true, length = 20)
    private Double tprice;
    @Column(name = "hot", nullable = true, length = 1)
    private int hot;
    @Column(name = "position", nullable = true, length = 11)
    private int position;
    @Column(name = "reward", nullable = true, length = 2)
    private int reward;
    @Column(name = "delivery", nullable = true, length = 2)
    private int delivery;
    @Column(name = "user_id", nullable = true, length = 40)
    private Long userId;
    @Column(name = "address_id", nullable = true, length = 40)
    private Long addressId;
    @Column(name = "create_by", nullable = true, length = 40)
    private String createBy;
    @Column(name = "create_time", nullable = true)
    private Date createTime;
    @Column(name = "update_by", nullable = true, length = 40)
    private String updateBy;
    @Column(name = "update_time", nullable = true)
    private Date updateTime;

//    public String getUrl1() {
//        return url1;
//    }
//
//    public void setUrl1(String url1) {
//        this.url1 = url1;
//    }
//
//    public String getUrl2() {
//        return url2;
//    }
//
//    public void setUrl2(String url2) {
//        this.url2 = url2;
//    }
//
//    public String getUrl3() {
//        return url3;
//    }
//
//    public void setUrl3(String url3) {
//        this.url3 = url3;
//    }
//
//    public String getUrl4() {
//        return url4;
//    }
//
//    public void setUrl4(String url4) {
//        this.url4 = url4;
//    }
//
//    private String url1;
//    private String url2;
//    private String url3;
//    private String url4;


    private String httpUrl;
    private String url;

//    private String httpUrl1;
//    private String httpUrl2;
//    private String httpUrl3;
//    private String httpUrl4;

    public String getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

//    public String getHttpUrl4() {
//        return httpUrl4;
//    }
//
//    public void setHttpUrl4(String httpUrl4) {
//        this.httpUrl4 = httpUrl4;
//    }
//
//    public String getHttpUrl1() {
//        return httpUrl1;
//    }
//
//    public void setHttpUrl1(String httpUrl1) {
//        this.httpUrl1 = httpUrl1;
//    }
//
//    public String getHttpUrl2() {
//        return httpUrl2;
//    }
//
//    public void setHttpUrl2(String httpUrl2) {
//        this.httpUrl2 = httpUrl2;
//    }
//
//    public String getHttpUrl3() {
//        return httpUrl3;
//    }
//
//    public void setHttpUrl3(String httpUrl3) {
//        this.httpUrl3 = httpUrl3;
//    }



    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCollectNum() {
        return collectNum;
    }

    public void setCollectNum(int collectNum) {
        this.collectNum = collectNum;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public int getGoodsStyle() {
        return goodsStyle;
    }

    public void setGoodsStyle(int goodsStyle) {
        this.goodsStyle = goodsStyle;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(int remainNum) {
        this.remainNum = remainNum;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public int getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(int saleNum) {
        this.saleNum = saleNum;
    }

    public Date getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public Double getSavePrice() {
        return savePrice;
    }

    public void setSavePrice(Double savePrice) {
        this.savePrice = savePrice;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStockNum() {
        return stockNum;
    }

    public void setStockNum(int stockNum) {
        this.stockNum = stockNum;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public Double getTprice() {
        return tprice;
    }

    public void setTprice(Double tprice) {
        this.tprice = tprice;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }


}