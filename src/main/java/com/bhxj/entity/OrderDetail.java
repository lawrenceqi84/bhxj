package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Table;

import javax.persistence.Entity;

@Entity
@Table(name = "order_detail_t")
public class OrderDetail {
	@Column(name = "id",primary = true)
	private String id;
	@Column(name = "ORDER_ID", nullable = true)
	private String orderId;
	@Column(name = "GOODS_ID", nullable = true)
	private Long goodsId;
	@Column(name = "GOODS_PRICE", nullable = true)
	private Double goodsPrice;
	@Column(name = "GOODS_TITLE", nullable = true)
	private String goodsTitle;
	@Column(name = "GOODS_IMAGE", nullable = true)
	private String goodsImage;
	@Column(name = "COUNT", nullable = true)
	private int count;
	@Column(name = "EMPLOYEE_NUM", nullable = true)
	private int employeeNum;
	@Column(name = "ADDRESS_DETAIL", nullable = true)
	private String addressDetail;
	@Column(name = "SUPPLIER_ID", nullable = true)
	private Long supplierId;
	@Column(name = "SUPPLIER_NAME", nullable = true)
	private String suppliserName;
	@Column(name = "EXPRESS_ID", nullable = true)
	private String expressId;
	@Column(name = "EXPRESS_NAME", nullable = true)
	private String expressName;
	@Column(name = "EXPRESS_NO", nullable = true)
	private String expressNo;
	@Column(name = "SEND_STATUS", nullable = true)
	private String sendStatus;
	@Column(name = "SEND_TIME", nullable = true)
	private String sendTime;
	@Column(name = "SEND_DETAIL", nullable = true)
	private String sendDetail;
	private Double tprice;

	public String getTitleShortName() {
		if (this.goodsTitle.length() > 23) {
			return goodsTitle.substring(0, 23) + "...";
		} else {
			return goodsTitle;
		}
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getExpressId() {
		return expressId;
	}

	public void setExpressId(String expressId) {
		this.expressId = expressId;
	}

	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsImage() {
		return goodsImage;
	}

	public void setGoodsImage(String goodsImage) {
		this.goodsImage = goodsImage;
	}

	public Double getGoodsPrice() {
		return goodsPrice;
	}

	public void setGoodsPrice(Double goodsPrice) {
		this.goodsPrice = goodsPrice;
	}

	public String getGoodsTitle() {
		return goodsTitle;
	}

	public void setGoodsTitle(String goodsTitle) {
		this.goodsTitle = goodsTitle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSendDetail() {
		return sendDetail;
	}

	public void setSendDetail(String sendDetail) {
		this.sendDetail = sendDetail;
	}

	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSuppliserName() {
		return suppliserName;
	}

	public void setSuppliserName(String suppliserName) {
		this.suppliserName = suppliserName;
	}

	public Double getTprice() {
		return tprice;
	}

	public void setTprice(Double tprice) {
		this.tprice = tprice;
	}

	public int getEmployeeNum() {
		return employeeNum;
	}

	public void setEmployeeNum(int employeeNum) {
		this.employeeNum = employeeNum;
	}
}

