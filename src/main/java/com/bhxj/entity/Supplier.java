package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Id;
import code.gen.annotations.Table;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by LawrenceQi on 15/11/18.
 */
@Entity
@Table(name = "supplier_t")
public class Supplier {
    @Id
    private Long id;
    @Column(name = "username", nullable = true, length = 50)
    private String username;
    @Column(name = "pwd", nullable = true, length = 50)
    private String pwd;
    @Column(name = "company", nullable = true, length = 10)
    private String company;
    @Column(name = "linkman", nullable = true, length = 20)
    private String linkman;
    @Column(name = "telephone", nullable = true, length = 11)
    private String telephone;
    @Column(name = "p", nullable = true, length = 10)
    private String p;
    @Column(name = "c", nullable = true, length = 10)
    private String c;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }



    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "d", nullable = true, length = 10)
    private String d;
    @Column(name = "address", nullable = true, length = 100)
    private String address;
    @Column(name = "start_time", nullable = true)
    private Date startTime;
    @Column(name = "end_time", nullable = true)
    private Date endTime;




}