package com.bhxj.entity;

/**
 * Created by Jean on 7/10/15.
 */
public class JsonResponse {
    private String msg;
    private boolean ok;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public JsonResponse(){}

    public JsonResponse(boolean ok, String msg) {
        this.ok = ok;
        this.msg = msg;
    }

    public static JsonResponse buildFail(String msg) {
        return new JsonResponse(false, msg);
    }

    public static JsonResponse buildPass(String msg) {
        return new JsonResponse(true, msg);
    }
}
