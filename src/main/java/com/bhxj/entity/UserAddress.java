package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Table;

import javax.persistence.Entity;

@Entity
@Table(name = "user_address_t")
public class UserAddress {
	@Column(name = "id",primary = true)
	private String id;
	@Column(name = "USER_ID", nullable = true)
	private Long userId;
	@Column(name = "TELEPHONE", nullable = true)
	private String telephone;
	@Column(name = "RECEIVER", nullable = true)
	private String receiver;
	@Column(name = "p", nullable = true)
	private String p;
	@Column(name = "c", nullable = true)
	private String c;
	@Column(name = "d", nullable = true)
	private String d;
	@Column(name = "PROPERTIES", nullable = true)
	private String properties;
	@Column(name = "DETAIL", nullable = true)
	private String detail;
	@Column(name = "SIZE", nullable = true)
	private Double size;
	@Column(name = "REAL_SIZE", nullable = true)
	private Double realSize;
	@Column(name = "PEOPLE", nullable = true)
	private Long people;
	@Column(name = "POSTCODE", nullable = true)
	private String postcode;

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getP() {
		return p;
	}

	public void setP(String p) {
		this.p = p;
	}

	public Long getPeople() {
		return people;
	}

	public void setPeople(Long people) {
		this.people = people;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Double getRealSize() {
		return realSize;
	}

	public void setRealSize(Double realSize) {
		this.realSize = realSize;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}



}
