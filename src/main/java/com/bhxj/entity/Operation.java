package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Table;

import javax.persistence.Entity;

@Entity
@Table(name = "operation_t")
public class Operation {
	@Column(name = "id",primary = true)
	private String id;
	@Column(name = "USER_ID", nullable = true)
	private Long userId;
	@Column(name = "GOODS_ID", nullable = true)
	private Long goodsId;
	@Column(name = "ORDER_ID", nullable = true)
	private String orderId;
	@Column(name = "ORDER_DETAIL_ID", nullable = true)
	private String orderDetailId;
	@Column(name = "OPERATION_DATE", nullable = true)
	private String operationDate;
	@Column(name = "PLAN_START_TIME", nullable = true)
	private String planStartTime;
	@Column(name = "PLAN_END_TIME", nullable = true)
	private String planEndTime;
	@Column(name = "PLAN_HOUR", nullable = true)
	private Double planHour;
	@Column(name = "PLAN_PRICE", nullable = true)
	private Double planPrice;
	@Column(name = "REAL_START_TIME", nullable = true)
	private String realStartTime;
	@Column(name = "REAL_END_TIME", nullable = true)
	private String realEndTime;
	@Column(name = "REAL_HOUR", nullable = true)
	private Double realHour;
	@Column(name = "REAL_PRICE", nullable = true)
	private Double realPrice;
	@Column(name = "PAY_STATUS", nullable = true)
	private int payStatus;
	@Column(name = "JOB_STATUS", nullable = true)
	private int jobStatus;
	private Double evaluate;
	@Column(name = "EVALUATE_CONTENT", nullable = true)
	private String evaluateContent;
	@Column(name = "OWN_EMPLOYEE_ID", nullable = true)
	private Long oweEmployeeId;

	public Long getOweEmployeeId() {
		return oweEmployeeId;
	}

	public void setOweEmployeeId(Long oweEmployeeId) {
		this.oweEmployeeId = oweEmployeeId;
	}

	public Double getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(Double evaluate) {
		this.evaluate = evaluate;
	}

	public String getEvaluateContent() {
		return evaluateContent;
	}

	public void setEvaluateContent(String evaluateContent) {
		this.evaluateContent = evaluateContent;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(int jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}

	public String getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}

	public String getPlanEndTime() {
		return planEndTime;
	}

	public void setPlanEndTime(String planEndTime) {
		this.planEndTime = planEndTime;
	}

	public Double getPlanHour() {
		return planHour;
	}

	public void setPlanHour(Double planHour) {
		this.planHour = planHour;
	}

	public Double getPlanPrice() {
		return planPrice;
	}

	public void setPlanPrice(Double planPrice) {
		this.planPrice = planPrice;
	}

	public String getPlanStartTime() {
		return planStartTime;
	}

	public void setPlanStartTime(String planStartTime) {
		this.planStartTime = planStartTime;
	}

	public String getRealEndTime() {
		return realEndTime;
	}

	public void setRealEndTime(String realEndTime) {
		this.realEndTime = realEndTime;
	}

	public Double getRealHour() {
		return realHour;
	}

	public void setRealHour(Double realHour) {
		this.realHour = realHour;
	}

	public Double getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	public String getRealStartTime() {
		return realStartTime;
	}

	public void setRealStartTime(String realStartTime) {
		this.realStartTime = realStartTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}










}
