package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Id;
import code.gen.annotations.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by LawrenceQi on 15/11/18.
 */
@Entity
@Table(name = "user_t")
public class User {
    @Id
    private Long id;
    @Column(name = "telephone", nullable = true, length = 20)
    private String telephone;
    @Column(name = "pwd", nullable = true, length = 50)
    private String pwd;
    @Column(name = "vcode", nullable = true, length = 6)
    private int vcode;
    @Column(name = "username", nullable = true, length = 50)
    private String username;
    @Column(name = "nickname", nullable = false, length = 50)
    private String nickname;
    @Column(name = "sex", nullable = true, length = 1)
    private String sex;
    @Column(name = "age", nullable = true, length = 3)
    private int age;
    @Column(name = "birthday", nullable = true, length = 20)
    private String birthday;
    @Column(name = "job", nullable = true, length = 50)
    private String job;
    @Column(name = "company", nullable = true, length = 50)
    private String company;
    @Column(name = "login_time", nullable = true, length = 50)
    private Date loginTime;//todo modify db column type
    @Column(name = "subscribe", nullable = false, length = 1)
    private boolean subscribe;
    @Column(name = "email", nullable = true, length = 255)
    private String email;
    @Column(name = "current_money")
    private double currentMoney;
    @Column(name = "discount")
    private double discount;
    @Column(name = "level", nullable = true, length = 50)
    private int level;
    @Column(name = "openid", nullable = false, length = 50)
    private String openid;
    @Column(name = "unionid", nullable = true, length = 50)
    private String unionid;
    @Column(name = "groupid", nullable = true, length = 11)
    private int groupid;
    @Column(name = "headimgurl", nullable = true, length = 255)
    private String headimgurl;
    @Column(name = "subscribe_time", nullable = true, length = 50)
    private Date subscribeTime;//todo modify db column type
    @Column(name = "remark", nullable = true, length = 50)
    private String remark;
    @Column(name = "language", nullable = true, length = 10)
    private String language;
    @Column(name = "country", nullable = true, length = 10)
    private String country;
    @Column(name = "province", nullable = true, length = 10)
    private String province;
    @Column(name = "city", nullable = true, length = 10)
    private String city;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    private String roles;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getCurrentMoney() {
        return currentMoney;
    }

    public void setCurrentMoney(double currentMoney) {
        this.currentMoney = currentMoney;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getVcode() {
        return vcode;
    }

    public void setVcode(int vcode) {
        this.vcode = vcode;
    }
    @Transient
    @JsonIgnore
    public List<String> getRoleList() {
        // 角色列表在数据库中实际以逗号分隔字符串存储，因此返回不能修改的List.
        return ImmutableList.copyOf(StringUtils.split(roles, ","));
    }

}