package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Table;

import javax.persistence.Entity;
import java.util.Map;

@Entity
@Table(name = "order_t")
public class Order {
	@Column(name = "id",primary = true)
	private String id;
	@Column(name = "USER_ID", nullable = true)
	private Long userId;
	private Double price;
	private Double tprice;
	@Column(name = "REAL_PRICE", nullable = true)
	private Double realPrice;
	@Column(name = "COUPON_ID", nullable = true)
	private String couponId;
	@Column(name = "COUPON_PRICE", nullable = true)
	private Double couponPrice;
	@Column(name = "ADDRESS_ID", nullable = true)
	private String addressId;
	@Column(name = "ADDRESS_DETAIL", nullable = true)
	private String addressDetail;
	@Column(name = "BUY_TIME", nullable = true)
	private String buyTime;
	@Column(name = "SEND_TIME", nullable = true)
	private String sendTime;
	@Column(name = "SEND_STATUS", nullable = true)
	private String sendStatus;
	@Column(name = "ORDER_STATUS", nullable = true)
	private String orderStatus;
	@Column(name = "PAY_WAY", nullable = true)
	private String payWay;
	@Column(name = "PAY_WAY_DETAIL", nullable = true)
	private String payWayDetail;
	@Column(name = "SHOPPING_COUNT", nullable = true)
	private int shoppingCount;
	@Column(name = "DELIVERY_BY_SELF_YN", nullable = true)
	private String deliveryBySelfYn="N";
	@Column(name = "BEST_EMPLOYEE_ID", nullable = true)
	private Long bestEmployeeId;
	@Column(name = "OWN_EMPLOYEE_ID", nullable = true)
	private Long ownEmployeeId;
	private Map<Long, OrderDetail> odMap;


	public Map<Long, OrderDetail> getOdMap() {
		return odMap;
	}

	public void setOdMap(Map<Long, OrderDetail> odMap) {
		this.odMap = odMap;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public Long getBestEmployeeId() {
		return bestEmployeeId;
	}

	public void setBestEmployeeId(Long bestEmployeeId) {
		this.bestEmployeeId = bestEmployeeId;
	}

	public String getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(String buyTime) {
		this.buyTime = buyTime;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getDeliveryBySelfYn() {
		return deliveryBySelfYn;
	}

	public void setDeliveryBySelfYn(String deliveryBySelfYn) {
		this.deliveryBySelfYn = deliveryBySelfYn;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getOwnEmployeeId() {
		return ownEmployeeId;
	}

	public void setOwnEmployeeId(Long ownEmployeeId) {
		this.ownEmployeeId = ownEmployeeId;
	}

	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	public String getPayWayDetail() {
		return payWayDetail;
	}

	public void setPayWayDetail(String payWayDetail) {
		this.payWayDetail = payWayDetail;
	}


	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public int getShoppingCount() {
		return shoppingCount;
	}

	public void setShoppingCount(int shoppingCount) {
		this.shoppingCount = shoppingCount;
	}

	public Double getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(Double couponPrice) {
		this.couponPrice = couponPrice;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	public Double getTprice() {
		return tprice;
	}

	public void setTprice(Double tprice) {
		this.tprice = tprice;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
