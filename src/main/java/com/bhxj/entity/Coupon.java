package com.bhxj.entity;

import code.gen.annotations.Column;
import code.gen.annotations.Table;

import javax.persistence.Entity;

/**
 * Created by LawrenceQi on 15/11/18.
 */
@Entity
@Table(name = "coupon_t")
public class Coupon {
    @Column(name = "id",primary = true)
    private String id;
    @Column(name = "user_id", nullable = true, length = 40)
    private String userId;
    @Column(name = "name", nullable = true, length = 100)
    private String name;
    @Column(name = "code", nullable = true, length = 8)
    private String code;
    @Column(name = "price", nullable = true)
    private Double price;
    @Column(name = "type", nullable = false, length = 20)
    private String type;
    @Column(name = "start_time", nullable = false, length = 10)
    private String startTime;

    @Column(name = "end_time", nullable = false, length = 10)
    private String endTIme;


    public String getEndTIme() {
        return endTIme;
    }

    public void setEndTIme(String endTIme) {
        this.endTIme = endTIme;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}