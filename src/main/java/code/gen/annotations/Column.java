package code.gen.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface Column {

    String name() default "";

    boolean unique() default false;

    boolean primary() default false;

    boolean nullable() default true;

    String columnDefinition() default "";

    int length() default 64;

    String decimalLength() default "10,2";
}

