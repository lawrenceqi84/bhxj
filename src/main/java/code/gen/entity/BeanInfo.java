package code.gen.entity;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jean on 10/22/15.
 */
public class BeanInfo {
    public static String GEN_OUT = "gen-out";
    private Config config;
    private String className;
    private String beanName;
    private String simpleName;
    private List<FieldInfo> fieldInfos = new ArrayList<FieldInfo>();
    private String tableName;
    private boolean hasPrimary;
    private FieldInfo primaryField = new FieldInfo();
    private String basePackageName;
    private String packageName;
    private String uncapitalizedSimpleName;

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public FieldInfo getPrimaryField() {
        return primaryField;
    }

    public void setPrimaryField(FieldInfo primaryField) {
        this.primaryField = primaryField;
    }

    public String getUncapitalizedSimpleName() {
        return uncapitalizedSimpleName;
    }

    public void setUncapitalizedSimpleName(String uncapitalizedSimpleName) {
        this.uncapitalizedSimpleName = uncapitalizedSimpleName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;

        if (!packageName.contains("entity")) {
            throw new RuntimeException("bean package name MUST BE  xx.xxxx.xx.entity.BeanName");
        }

        this.setBasePackageName(packageName.substring(0, packageName.indexOf("entity") - 1));
    }

    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String basePackageName) {
        this.basePackageName = basePackageName;
    }

    public boolean isHasPrimary() {
        return hasPrimary;
    }

    public void setHasPrimary(boolean hasPrimary) {
        this.hasPrimary = hasPrimary;
    }

    public void addFileInfo(FieldInfo fieldInfo) {
        fieldInfos.add(fieldInfo);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<FieldInfo> getFieldInfos() {
        return fieldInfos;
    }

    public void setFieldInfos(List<FieldInfo> fieldInfos) {
        this.fieldInfos = fieldInfos;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.uncapitalizedSimpleName = StringUtils.uncapitalize(simpleName);
        this.simpleName = simpleName;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
