package code.gen.entity;

/**
 * Created by Jean on 10/23/15.
 */
public class Config {
    private String bean;
    private String outRootDir;
    private String sqlDir;
    private String mapperDir;
    private String serviceDir;
    private String daoDir;

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public String getOutRootDir() {
        return outRootDir;
    }

    public void setOutRootDir(String outRootDir) {
        this.outRootDir = outRootDir;
    }

    public String getSqlDir() {
        return sqlDir;
    }

    public void setSqlDir(String sqlDir) {
        this.sqlDir = sqlDir;
    }

    public String getMapperDir() {
        return mapperDir;
    }

    public void setMapperDir(String mapperDir) {
        this.mapperDir = mapperDir;
    }

    public String getServiceDir() {
        return serviceDir;
    }

    public void setServiceDir(String serviceDir) {
        this.serviceDir = serviceDir;
    }

    public String getDaoDir() {
        return daoDir;
    }

    public void setDaoDir(String daoDir) {
        this.daoDir = daoDir;
    }
}
