package code.gen.entity;

/**
 * Created by Jean on 10/22/15.
 */
public class FieldInfo {
    private String name;
    private Class type;
    private String column;
    private int dbTypeLength;
    private String dbType;
    private String javaType;
    private boolean idColumn;
    private boolean nullable;
    private boolean primary = false;
    private String dbDecimalLength;

    public String getDbDecimalLength() {
        return dbDecimalLength;
    }

    public void setDbDecimalLength(String dbDecimalLength) {
        this.dbDecimalLength = dbDecimalLength;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public boolean isIdColumn() {
        return idColumn;
    }

    public void setIdColumn(boolean idColumn) {
        this.idColumn = idColumn;
    }

    public int getDbTypeLength() {
        return dbTypeLength;
    }

    public void setDbTypeLength(int dbTypeLength) {
        this.dbTypeLength = dbTypeLength;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }
}
