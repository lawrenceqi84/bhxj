package code.gen.util;

import code.gen.entity.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtil {
    public static String genBaseDir(Config config, String basePackageName) {
        String base = config.getOutRootDir() + File.separator + basePackageName.replace(".", "/");
//        doCreateDir(base);

        // sql dir
//        doCreateDir(config.getOutRootDir() + File.separator + config.getSqlDir());

        // service dir
//        doCreateDir(base + File.separator + config.getServiceDir());

        return base;
    }

    private static boolean doCreateDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    public static void genFile(String filePath, String content) {
        System.out.println(filePath);
        File file = new File(filePath);
        File dir = new File(file.getParent());
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(content.getBytes("utf-8"));
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
