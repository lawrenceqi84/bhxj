package code.gen;

import code.gen.annotations.Column;
import code.gen.annotations.Id;
import code.gen.annotations.Table;
import code.gen.entity.BeanInfo;
import code.gen.entity.Config;
import code.gen.entity.FieldInfo;
import code.gen.util.FileUtil;
import com.google.gson.Gson;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Date;

public class Generator {
    private static final Logger logger = LoggerFactory.getLogger(Generator.class);
    public static final String DB_TYPT_VARCHAR = "VARCHAR";
    public static final String DB_TYPE_INT = "INT";
    public static final String DB_TYPE_TINYINT = "TINYINT";
    public static final String DB_TYPE_TIMESTAMP = "TIMESTAMP";
    public static final String DB_TYPE_DECIMAL = "DECIMAL";
    public static final String DB_TYPE_TEXT = "TEXT";
    public static final String JAVA_TYPE_STRING = "String";
    public static final String JAVA_TYPE_LONG = "Long";

    public static void main(String[] args) {
        Config config = readConfigFile();
        String className = config.getBean();

        BeanInfo beanInfo = new BeanInfo();
        beanInfo.setConfig(config);
        logger.info("clazz name: {}", className);
        try {
            Class clazz = Class.forName(className);

            beanInfo.setPackageName(clazz.getPackage().getName());
            beanInfo.setSimpleName(clazz.getSimpleName());
            logger.info("class package:{}", clazz.getPackage().getName());
            logger.info("base package:{}", beanInfo.getBasePackageName());

            Table table = (Table) clazz.getAnnotation(Table.class);
            if (table != null) {
                beanInfo.setTableName(table.name());
            } else {
                beanInfo.setTableName(beanInfo.getSimpleName().toLowerCase());
            }
            logger.info("table name: {}", beanInfo.getTableName());

            for (Field field : clazz.getDeclaredFields()) {
                FieldInfo fieldInfo = new FieldInfo();
                beanInfo.addFileInfo(fieldInfo);

                fieldInfo.setName(field.getName());
                fieldInfo.setType(field.getType());

                if (field.getType().equals(String.class)) {
                    fieldInfo.setDbType(DB_TYPT_VARCHAR);
                    fieldInfo.setDbTypeLength(32);
                    fieldInfo.setJavaType(JAVA_TYPE_STRING);
                } else if (field.getType().equals(Long.class)) {
                    fieldInfo.setDbType(DB_TYPE_INT);
                    fieldInfo.setDbTypeLength(11);
                    fieldInfo.setJavaType(JAVA_TYPE_LONG);
                } else if (field.getType().equals(int.class)) {
                    fieldInfo.setDbType(DB_TYPE_INT);
                    fieldInfo.setDbTypeLength(11);
                    fieldInfo.setJavaType(JAVA_TYPE_LONG);
                } else if (field.getType().equals(boolean.class)) {
                    fieldInfo.setDbType(DB_TYPE_TINYINT);
                    fieldInfo.setDbTypeLength(1);
                } else if (field.getType().equals(Date.class)) {
                    fieldInfo.setDbType(DB_TYPE_TIMESTAMP);
                } else if (field.getType().equals(double.class)) {
                    fieldInfo.setDbType(DB_TYPE_DECIMAL);
                }

                Column c = field.getAnnotation(Column.class);
                if (c != null) {
                    // 处理自定义字段名称
                    if ("".equals(c.name())) {
                        fieldInfo.setColumn(field.getName());
                    } else {
                        fieldInfo.setColumn(c.name().toLowerCase());
                    }


                    // 处理字段类型，特别处理数据库Text类型字段
                    if (DB_TYPE_TEXT.equals(c.columnDefinition())) {
                        fieldInfo.setDbType(DB_TYPE_TEXT);
                    }

                    // 处理字段长度, 特别处理Decimal类型长度
                    if ("DECIMAL".equals(fieldInfo.getDbType())) {
                        fieldInfo.setDbDecimalLength(c.decimalLength());
                    } else {
                        fieldInfo.setDbTypeLength(c.length());
                    }

                    // 处理是否为空
                    fieldInfo.setNullable(c.nullable());

                    // 处理主键
                    if (c.primary()) {
                        if (beanInfo.isHasPrimary()) {
                            logger.error("Bean只能设置一个主键,用Id或者Column的primary");
                            System.exit(0);
                        }
                        beanInfo.setPrimaryField(fieldInfo);
                        beanInfo.setHasPrimary(true);
                        fieldInfo.setPrimary(true);
                    }
                } else {
                    fieldInfo.setColumn(field.getName().toLowerCase());
                    fieldInfo.setNullable(true);
                    fieldInfo.setPrimary(false);
                }

                Id id = field.getAnnotation(Id.class);
                if (id != null) {
                    beanInfo.setHasPrimary(true);
                    beanInfo.setPrimaryField(fieldInfo);

                    fieldInfo.setIdColumn(true);
                    fieldInfo.setNullable(false);
                    fieldInfo.setPrimary(true);
                }

                logger.info("field: name={}, type={}, column={}, columnType={}, columnLength={}", fieldInfo.getName(), fieldInfo.getType(), fieldInfo.getColumn(), fieldInfo.getDbType(), fieldInfo.getDbTypeLength());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (!hasPrimary(beanInfo)) {
            logger.error("Bean必须设置主键,用Id或者Column的primary");
            System.exit(0);
        }

        String simpleName = beanInfo.getSimpleName();
        try {
            generateSQL(config.getSqlDir() + File.separator + simpleName + ".sql", beanInfo);
            generateDao(config.getDaoDir() + File.separator + simpleName + "Dao.java", beanInfo);
            generateService(config.getServiceDir() + File.separator + simpleName + "Service.java", beanInfo);
            generateMapper(config.getMapperDir() + File.separator + simpleName + "Mapper.xml", beanInfo);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static boolean hasPrimary(BeanInfo beanInfo) {
        if (!beanInfo.isHasPrimary()) {
            return false;
        }
        return true;
    }

    public static void generateMapper(String mapperFile, BeanInfo beanInfo) throws Exception{
        generateCode("tpls/mapper.vm", mapperFile, beanInfo);
    }

    public static void generateService(String serviceFile, BeanInfo beanInfo) throws Exception{
        generateCode("tpls/service.vm", serviceFile, beanInfo);
    }

    public static void generateDao(String daoFile, BeanInfo beanInfo) throws Exception{
        generateCode("tpls/dao.vm", daoFile, beanInfo);
    }

    public static void generateSQL(String sqlFile, BeanInfo beanInfo) throws Exception{
        generateCode("tpls/sql.vm", sqlFile, beanInfo);
    }


    public static void generateCode(String template, String fileout, BeanInfo beanInfo) throws Exception{
        VelocityEngine ve = new VelocityEngine();
        ve.init();
        Template t = ve.getTemplate(template);
        VelocityContext context = new VelocityContext();
        context.put("beanInfo", beanInfo);
        StringWriter writer = new StringWriter();
        t.merge(context, writer);

        FileUtil.genFile(fileout, writer.getBuffer().toString());
    }

    public static Config readConfigFile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("config.json");
            byte[] buf = new byte[fileInputStream.available()];
            fileInputStream.read(buf);
            return new Gson().fromJson(new String(buf), Config.class);
        } catch (FileNotFoundException e) {
            logger.error("没有找到配置文件");
            throw new RuntimeException(e);
        } catch (IOException e) {
            logger.error("解析配置文件错误");
            throw new RuntimeException(e);
        }
    }
}
